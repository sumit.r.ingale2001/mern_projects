import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Box, Button, styled } from '@mui/material';
import { ShoppingCart as Cart, FlashOn as Flash } from '@mui/icons-material'
import { addToCart } from '../../redux/actions/cartAction';

const LeftContainer = styled(Box)(({ theme }) => ({
    minWidth: "40%",
    padding: "40px 0 0 80px",
    textAlign: "center",
    [theme.breakpoints.down("lg")]: {
        padding: "20px 40px",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    }
}))


const Image = styled("img")({
    padding: 15
})

const StyledButton = styled(Button)(({ theme }) => ({
    width: "48%",
    height: 50,
    borderRadius: 2,
    [theme.breakpoints.down('lg')]: {
        width: "46%"
    },
    [theme.breakpoints.down("sm")]: {
        width: "48%"
    }
}))

const ActionItem = ({ product }) => {

    const navigate = useNavigate();
    const dispatch = useDispatch();
    const { id } = product;

    const [quantity, setQuantity] = useState(1)


    const addItemToCart = () => {
        dispatch(addToCart(id, quantity))
        navigate('/cart');
    }

    return (
        <LeftContainer>
            <Box style={{
                padding: "15px 20px",
                border: "1px solid #f0f0f0",
                width: "90%",
                textAlign: "center",
            }} >
                <Image src={product.detailUrl} alt="product" />
            </Box>
            <Box style={{ width: "100%" }}>
                <StyledButton variant='contained' style={{ marginRight: 10, background: "#ff9f00" }} onClick={() => addItemToCart()} >  <Cart /> Add to Cart</StyledButton>
                <StyledButton  variant='contained' style={{ background: "#fb5413" }} > <Flash /> Buy Now</StyledButton>
            </Box>
        </LeftContainer>
    )
}

export default ActionItem;
