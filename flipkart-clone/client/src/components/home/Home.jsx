import React, { useEffect } from 'react'
import { Box, styled } from '@mui/material';
import Navbar from './Navbar';
import Banner from './Banner';
import { getProducts } from '../../redux/actions/productAction';
import { useDispatch, useSelector } from 'react-redux';
import Slide from './Slide'
import MidSlide from './MidSlide';
import MidSection from './MidSection';

const Container = styled(Box)`
padding:10px;
background: #F2F2F2;


`

const Home = () => {

    const { products } = useSelector(state => state.getProducts);

    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(getProducts());
    }, [dispatch]);
    return (
        <Box>

            <Navbar />
            <Container>
                <Banner />
                <MidSlide products={products} title="Deal of the Day" timer={true} />
                <MidSection/>
                <Slide products={products} title="Discount for you" timer={false} />
                <Slide products={products} title="Top selection" timer={false} />
            </Container>
        </Box>
    )
}

export default Home
