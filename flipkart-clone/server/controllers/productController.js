import Product from '../models/productSchema.js';

export const getProducts = async (req, res) => {
    try {
        let products = await Product.find({});
        res.status(200).json(products);
    } catch (error) {
        res.status(500).send({message : error.message});
    }
}

export const getProductById = async (req,res)=>{
try {
    const id = req.params.id;
    let product = await Product.findOne({"id": id});
    res.status(200).json(product)
} catch (error) {
    res.status(500).send({message : error.message});
}
}