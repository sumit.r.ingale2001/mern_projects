import mongoose from 'mongoose';

const Connection = async (USERNAME,PASSWORD)=>{
    try {
        const URL = `mongodb+srv://${USERNAME}:${PASSWORD}@ecommerce.3udb9m0.mongodb.net/flipkart-clone?retryWrites=true&w=majority`;
        await mongoose.connect(URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        console.log("Database connected successfully")
    } catch (error) {
        console.log("Error while connecting to database", error.message)
    }

}

export default Connection;