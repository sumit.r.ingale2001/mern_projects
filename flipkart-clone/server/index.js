import express from "express";
import Connection from "./database/db.js";
import dotenv from 'dotenv';
import DefaultData from "./default.js";
import Router from './routes/route.js';
import cors from 'cors';
import bodyParser from "body-parser";

dotenv.config();
const PASSWORD = process.env.DB_PASSWORD;
const USERNAME = process.env.DB_USERNAME;

const app = express();
app.use(bodyParser.json({ extended: true }))
app.use(bodyParser.urlencoded({extended:true}))
app.use(cors());

const port = process.env.PORT || 8000;


Connection(USERNAME, PASSWORD);

app.listen(port, () => console.log(`Successfully listening to port ${port}`))
app.use("/", Router);

DefaultData();