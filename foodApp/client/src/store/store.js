import { configureStore } from "@reduxjs/toolkit";
import RestaurantSlice from "./restaurant";


export const store = configureStore({
    reducer: {
        restaurant: RestaurantSlice.reducer
    }
})