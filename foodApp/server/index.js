import express from 'express'
import cors from "cors"
import cookieParser from 'cookie-parser'
import Connection from './database/db.js';
import userRoutes from './routes/userRoutes.js'


// port for the express app 
const PORT = 8000;

// initializing express app 
const app = express();

app.use(express.json());
app.use(cors({
    origin: ["http://localhost:5173"],
    credentials: true
}))
app.use(cookieParser())

app.use("/user", userRoutes)

//calling the database connection 
Connection();
// starting the app 
app.listen(PORT, () => console.log(`server started on port ${PORT}`))