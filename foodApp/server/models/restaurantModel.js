import mongoose from "mongoose";

// creating structure for restaurant 

const restaurantModel = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    username: {
        type: String,
        required: true,
        unique: true,
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        unique: true,
        required: true
    },
    address: {
        type: String,
        required: true,
    },
    openingTime: {
        type: String,
        required: true
    },
    closingTime: {
        type: String,
        required: true,
    },
    role: {
        type: String,
        required: true,
        default: "admin"
    },
    products: {
        type: [
            { title: { type: String, required: true, max: 50 } },
            { desc: { type: String, required: true, max: 200 } },
            { img: { type: String, required: true } },
            { prices: { type: Array, required: true } },
            {
                extra: {
                    type: [
                        {
                            text: { type: String, required: true },
                            price: { type: Number, required: true }
                        }
                    ]
                }
            }
        ]
    }
},
    { timestamps: true }
)

const Restaurant = mongoose.model("restaurant", restaurantModel);

export default Restaurant;