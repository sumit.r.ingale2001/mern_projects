import mongoose from "mongoose";

const Connection = async (username= "googleDocs", password = 'googledocs123')=>{
    try {
        const URL = `mongodb+srv://${username}:${password}@google-docs-clone.n1rshxa.mongodb.net/google-docs-clone?retryWrites=true&w=majority`;
        await mongoose.connect(URL,{
            useNewUrlParser: true,
            useUnifiedTopology: true,
        })
        console.log("Connected to database");
    } catch (error) {
        console.log("Erroe while connecting to database" ,error);
    }
}

export default Connection;