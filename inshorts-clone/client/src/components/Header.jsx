import React from 'react'
import { AppBar, Toolbar, styled } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';

const StyledAppbar = styled(AppBar)`
background-color: #fff;
`
const Menu = styled(MenuIcon)`
color: #000;
cursor: pointer;
`

const Image = styled("img")`
width:100px;
margin: 0 auto;
`
const StyledToolbar = styled(Toolbar)`
display: flex;
align-items: center;
`

const Header = () => {
    const url = 'https://assets.inshorts.com/website_assets/images/logo_inshorts.png';
    return (
        <StyledAppbar position='static'>
            <StyledToolbar>
                <Menu />
                <Image src={url} alt="logo" />
            </StyledToolbar>
        </StyledAppbar>
    )
}

export default Header
