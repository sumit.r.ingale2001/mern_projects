import React from 'react'
import { Box, Typography, styled } from '@mui/material'

const Container = styled(Box)(({ theme }) => ({
    backgroundColor: "#f44336",
    color: "#fff",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    height: 48,
    padding: 5,
    marginBottom: 30,
    [theme.breakpoints.down("md")]: {
        display: "none"
    }
}));

const Image = styled("img")`
height:34px;
`

const Text = styled(Typography)`
font-size : 14px;
font-weight: 300;
margin-left:10px;
`

const InfoHeader = () => {
    const appleStore = 'https://assets.inshorts.com/website_assets/images/appstore.png';
    const googleStore = 'https://assets.inshorts.com/website_assets/images/playstore.png';
    return (
        <Container>
            <Text>
                For the best experience use inshorts app in your smartphone.
            </Text>
            <Box style={{ display: "flex" }} >
                <Image src={appleStore} alt="appleStore logo" />
                <Image src={googleStore} alt="googleStore logo" />
            </Box>
        </Container>
    )
}

export default InfoHeader
