import mongoose from 'mongoose';
import dotenv from 'dotenv'

dotenv.config();

const USERNAME = process.env.DB_USERNAME 
const PASSWORD = process.env.DB_PASSWORD;

const Connection = async ()=>{
    try {
        const URL = `mongodb+srv://${USERNAME}:${PASSWORD}@inshortappclone.iwtmf0x.mongodb.net/inShortAppClone?retryWrites=true&w=majority`
        await mongoose.connect(URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        })
        console.log("Database Connected")

    } catch (error) {
        console.log("error while connecting to the database", error)
    }
}

export default Connection;