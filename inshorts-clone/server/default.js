import {data} from './constants/data.js'
import News from './models/newsSchema.js'


const DefaultData = async () => {
    try {
        await News.insertMany(data)
        console.log("Data imported")
    } catch (error) {
        console.log(error.message, "Error in default data")
    }
}

export default DefaultData;