import express from "express";
import Connection from "./database/db.js";
import DefaultData from "./default.js";
import Route from './routes/route.js'
import cors from 'cors'

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use(cors())

const port = 8000 || process.env.PORT;

app.use("/", Route)


Connection();

app.listen(port, ()=> console.log(`Successfully listening to port ${port}`))

DefaultData()