import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import AddUser from './components/AddUser'
import AllUser from './components/AllUser'
import Home from './components/Home'
import Edit from './components/EditUser'
import Navbar from './components/Navbar'


function App() {
  return (
    <Router>
      <Navbar />
      <Routes>
        <Route exact path='/' element={<Home />} />
        <Route exact path='/add' element={<AddUser />} />
        <Route exact path='/all' element={<AllUser />} />
        <Route exact path='/edit/:id' element={<Edit />} />
      </Routes>
    </Router>
  )
}

export default App
