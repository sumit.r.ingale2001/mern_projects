

import { Box, Button, Table, TableBody, TableCell, TableHead, TableRow } from '@mui/material'
import { deleteUser, getUsers } from '../api'
import { useEffect, useState } from 'react'
import styled from '@emotion/styled'
import { Link, useNavigate } from 'react-router-dom'


const StyledTable = styled(Table)`
width:80%;
backdrop-filter:blur(10px);
background:rgba(225,225,225,0.5);
margin:50px auto;
& tbody{
    background:#242428;
};
& td{
    color:white
}
`

const THead = styled(TableHead)`
background:black;
& tr> th{
    color:white;
}
`;

const Container = styled(Box)`

margin:50px auto;
max-width:1000px;
width:80%;
display:flex;
align-items:center;
justify-content:center;
padding:10px;
& > button{
    margin-left:15px;
}
`







const AllUser = () => {
    const [users, setUsers] = useState([]);
    const navigate = useNavigate()
    useEffect(() => {
        getAllUsers();
    }, [])
    const getAllUsers = async () => {
        let { data } = await getUsers();
        setUsers(data)
    }

    const handleDelete = async (id) => {
        await deleteUser(id);
        getAllUsers();

    }
    return (
        <>
            {
                users.length === 0 ? (
                    <Container>
                        <h3>No users added</h3>
                        <Button
                            onClick={() => navigate('/add')}
                            variant='contained'
                            size='small'
                        >Add users</Button>
                    </Container>
                ) : (
                    <StyledTable
                        xs={{ marginTop: "5rem" }}
                    >
                        <THead>
                            <TableRow>
                                <TableCell>Id</TableCell>
                                <TableCell>Name</TableCell>
                                <TableCell>Username</TableCell>
                                <TableCell>Email</TableCell>
                                <TableCell>Phone</TableCell>
                                <TableCell>Edit</TableCell>
                                <TableCell>Delete</TableCell>
                            </TableRow>
                        </THead>
                        <TableBody>
                            {
                                users.map((user) => (
                                    <TableRow key={user._id} >
                                        <TableCell>{user._id}</TableCell>
                                        <TableCell>{user.name}</TableCell>
                                        <TableCell>{user.username}</TableCell>
                                        <TableCell>{user.email}</TableCell>
                                        <TableCell>{user.phone}</TableCell>
                                        <TableCell>
                                            <Button variant='contained' size='small'
                                                component={Link} to={`/edit/${user._id}`}
                                            >Edit</Button>
                                        </TableCell>
                                        <TableCell>
                                            <Button variant='contained' size='small' color='error'
                                                onClick={() => handleDelete(user._id)}
                                            >Delete</Button>
                                        </TableCell>
                                    </TableRow>
                                ))
                            }
                        </TableBody>
                    </StyledTable>
                )
            }
        </>
    )
}

export default AllUser
