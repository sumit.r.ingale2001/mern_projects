import { Box, Button, TextField, Typography, styled } from "@mui/material"
import { useEffect, useState } from "react"
import {
    editUser,
    getUser
} from "../api"
import { useNavigate, useParams } from 'react-router-dom'

const Container = styled(Box)`
height:90vh;
width:100vw;
display:flex;
justify-content:center;
align-items:center;
overflow:hidden;

&>div{
    background:rgba(225,225,225,0.4);
    backdrop-filter:blur(20px);
    border-radius:5px;
    padding:20px 30px;
    display:flex;
    max-width:500px;
    width:80%;
    flex-direction:column;
    & > div{
        margin:10px 0;
    }
    & > h5{
        text-align:center;
        margin:10px 0;
    }
    & > button{
        margin:10px 0;
    }
}
`

const EditUser = () => {

    const navigate = useNavigate();
    const { id } = useParams();

    const [formValues, setFormValues] = useState({
        name: '',
        username: '',
        email: '',
        phone: ''
    })


    useEffect(() => {
        loadUserDetails();
    }, [])

    const loadUserDetails = async () => {
        const { data } = await getUser(id);
        setFormValues(data)
    }

    const handleClick = async () => {
        await editUser(id, formValues);
        navigate('/all')

    }


    return (
        <Container>
            <Box>
                <Typography variant="h5">Edit user</Typography>
                <Box>
                    <TextField
                        fullWidth
                        required
                        type="text"
                        label="Name"
                        name="name"
                        onChange={(e) => setFormValues({ ...formValues, [e.target.name]: e.target.value })}
                        value={formValues.name}
                    />
                </Box>
                <Box>
                    <TextField
                        fullWidth
                        required
                        label="Username"
                        name="username"
                        type="text"
                        onChange={(e) => setFormValues({ ...formValues, [e.target.name]: e.target.value })}
                        value={formValues.username}
                    />
                </Box>
                <Box>
                    <TextField
                        fullWidth
                        required
                        label="Email"
                        name="email"
                        type="email"
                        onChange={(e) => setFormValues({ ...formValues, [e.target.name]: e.target.value })}
                        value={formValues.email}
                    />
                </Box>
                <Box>
                    <TextField
                        fullWidth
                        required
                        label="Phone"
                        type="number"
                        name="phone"
                        onChange={(e) => setFormValues({ ...formValues, [e.target.name]: e.target.value })}
                        value={formValues.phone}
                    />
                </Box>
                <Button
                    onClick={() => handleClick()}
                    variant="contained"
                >Edit User</Button>
            </Box>
        </Container>
    )
}

export default EditUser
