import {styled , AppBar, Toolbar } from '@mui/material'

const links = [
    { name: "CRUD-APP", to: "/" },
    { name: "All User", to: "/all" },
    { name: "Add User", to: "/add" },
]
import { Link } from 'react-router-dom'


const Header = styled(AppBar)`
background:black;
color:white;
& div > a{
    text-decoration:none;
    margin:0 10px;
    color:white;
}
`

const Navbar = () => {
    return (
        <>
            <Header position='static'>
                <Toolbar>
                    {
                        links.map((link) => (
                            <Link to={link.to} key={link.name} >{link.name}</Link>
                        ))
                    }
                </Toolbar>
            </Header>
        </>
    )
}

export default Navbar
