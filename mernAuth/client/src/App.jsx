import Login from "./components/Login"
import Signup from "./components/Signup"
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Dashboard from "./components/Dashboard"
import Home from "./components/Home"

function App() {

  return (
    <>
      <div className="container d-flex justify-content-center align-items-center">
        <BrowserRouter>
          <Routes>
            <Route path="/" exact element={<Signup />} />
            <Route path="/login" exact element={<Login />} />
            <Route path="/dashboard" exact element={<Dashboard />} />
            <Route path="/home" exact element={<Home />} />
          </Routes>
        </BrowserRouter>
      </div>
    </>
  )
}

export default App
