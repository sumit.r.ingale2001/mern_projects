import axios from 'axios';


axios.defaults.withCredentials = true;
const url = "http://localhost:8000"

export const registerUser = async (data) => {
    try {
        return await axios.post(`${url}/register`, data);
    } catch (error) {
        console.log(error, 'error while calling registerUser api');
    }
}


export const loginUser = async (data) => {
    try {
        return await axios.post(`${url}/login`, data)
    } catch (error) {
        console.log(error, 'error while calling loginUser api')
    }
}

export const verifyAdmin = async () => {
    try {
        return await axios.get(`${url}/admin`);
    } catch (error) {
        console.log(error, 'error while calling verfiyAdmin api');
    }
}