import { useEffect } from "react";
import { verifyAdmin } from "../api"
import { useNavigate } from "react-router-dom";

const Dashboard = () => {

    const navigate = useNavigate()

    useEffect(() => {
        admin()
    }, [])

    const admin = async () => {
        const { data } = await verifyAdmin();
        console.log(data)
        if (data.Status === "Success") {
            alert("Admin login");
        } else {
            navigate('/')
        }
    }


    return (
        <div>
            Dashboard
        </div>
    )
}

export default Dashboard
