import { useState } from "react"
import { Link, useNavigate } from "react-router-dom";
import { loginUser } from "../api";




const Login = () => {

    const navigate = useNavigate()

    const [formValues, setFormValues] = useState({
        email: "",
        password: "",
    })

    const handleClick = async (e) => {
        e.preventDefault();
        const response = await loginUser(formValues);
        console.log(response)
        if (!response) {
            alert("Invalid credentials");
        } else {
            alert("Login successfull")
            if (response.data.Status === "Success") {
                if (response.data.role === 'admin') {
                    navigate("/dashboard")
                } else {
                    navigate("/home")
                }
            }
        }
    }

    return (
        <>
            <div className="card" style={{ width: "80%", maxWidth: "600px", padding: "1rem 2rem" }}>
                <h5 className="text-center" >Login</h5>
                <div className="card-body">
                    <form className="d-flex flex-column justify-content-center">

                        <div className="input-group mb-3">
                            <input type="email"
                                name="email"
                                onChange={(e) => setFormValues({ ...formValues, [e.target.name]: e.target.value })}
                                className="form-control"
                                placeholder="Email"
                                aria-label="Email"
                                aria-describedby="basic-addon1" />
                        </div>

                        <div className="input-group mb-3">
                            <input type="password"
                                name="password"
                                onChange={(e) => setFormValues({ ...formValues, [e.target.name]: e.target.value })}
                                className="form-control"
                                placeholder="Password"
                                aria-label="Username"
                                aria-describedby="basic-addon1" />
                        </div>

                        <button
                            onClick={handleClick}
                            className="btn btn-primary">
                            Login
                        </button>
                        <div className="mt-3">
                            <p>Not a member? <Link to="/">Signup</Link></p>
                        </div>
                    </form>
                </div>
            </div>
        </>
    )
}

export default Login
