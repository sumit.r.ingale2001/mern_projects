import { useState } from "react"
import { Link, useNavigate } from "react-router-dom";
import { registerUser } from "../api";




const Signup = () => {

    const navigate = useNavigate()

    const [formValues, setFormValues] = useState({
        name: "",
        email: "",
        password: "",
        role: ""
    })

    const handleClick = async (e) => {
        e.preventDefault();
        if (formValues.name && formValues.email && formValues.password && formValues.role !== "") {
            const response = await registerUser(formValues)
            if (!response) {
                alert("Enter correct credentials")
            } else {
                alert("Account created successfully");
                navigate("/login")
            }
        } else {
            alert("Fields cannot be empty")
        }
    }

    return (
        <>
            <div className="card" style={{ width: "80%", maxWidth: "600px", padding: "1rem 2rem" }}>
                <h5 className="text-center" >Signup</h5>
                <div className="card-body">
                    <form className="d-flex flex-column justify-content-center">
                        <div className="input-group mb-3">
                            <input type="text"
                                required
                                name="name"
                                onChange={(e) => setFormValues({ ...formValues, [e.target.name]: e.target.value })}
                                className="form-control"
                                placeholder="Name"
                                aria-label="Username"
                                aria-describedby="basic-addon1" />
                        </div>
                        <div className="input-group mb-3">
                            <input type="email"
                                required
                                name="email"
                                onChange={(e) => setFormValues({ ...formValues, [e.target.name]: e.target.value })}
                                className="form-control"
                                placeholder="Email"
                                aria-label="Email"
                                aria-describedby="basic-addon1" />
                        </div>
                        <div className="input-group mb-3">
                            <input type="password"
                                required
                                name="password"
                                onChange={(e) => setFormValues({ ...formValues, [e.target.name]: e.target.value })}
                                className="form-control"
                                placeholder="Password"
                                aria-label="Username"
                                aria-describedby="basic-addon1" />
                        </div>
                        <div className="input-group mb-3">
                            <select className="form-select"
                                required
                                name="role"
                                onChange={(e) => setFormValues({ ...formValues, [e.target.name]: e.target.value })} >
                                <option selected>Choose</option>
                                <option value="user">User</option>
                                <option value="admin">Admin</option>
                            </select>
                        </div>
                        <button
                            onClick={handleClick}
                            className="btn btn-primary">
                            Signup
                        </button>
                        <div className="mt-3">
                            <p>Already a member? <Link to="/login">Login</Link></p>
                        </div>
                    </form>
                </div>
            </div>
        </>
    )
}

export default Signup;
