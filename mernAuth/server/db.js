import mongoose from "mongoose";


const URL = "mongodb://127.0.0.1:27017/auth";

const Connection = async () => {
    try {
        await mongoose.connect(URL, {
            useUnifiedTopology: true,
            useNewUrlParser: true
        })
        console.log("database connected successfully")
    } catch (error) {
        console.log(error, 'error while connecting to database');
    }
}

export default Connection;