import express from 'express';
import cors from 'cors';
import Connection from './db.js';
import cookieParser from 'cookie-parser';
import userRoutes from './userRoute.js'

const PORT = 8000;

const app = express();

app.use(express.json());
app.use(cors({
    origin:["http://localhost:5173"],
    methods:["GET","POST"],
    credentials:true
}));
app.use(cookieParser())


app.use("/", userRoutes)


Connection()
app.listen(PORT, () => console.log(`server started on port ${PORT}`))