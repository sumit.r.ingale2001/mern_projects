import bcrypt from 'bcrypt';
import User from './userModel.js'
import jwt from 'jsonwebtoken'

const hashPass = async (pass) => {
    return await bcrypt.hash(pass, 10);
}

const comparePass = async (pass, dataPass) => {
    return await bcrypt.compare(pass, dataPass)
}



export const registerUser = async (req, res) => {
    try {
        const { email, password, name, role } = req.body;

        const user = await User.findOne({ email: email });

        if (user) {
            return res.json({ message: "User already exist" });
        } else {
            const hashedPassword = await hashPass(password);
            const newUser = new User({
                name,
                role,
                email,
                password: hashedPassword
            });
            await newUser.save();
            return res.status(200).json({user:user, message:"User registered", Status:"Success"})
        }

    } catch (error) {
        res.status(500).json(error, 'error while registering the user')
    }
}

export const loginUser = async (req, res) => {
    try {
        const { email, password } = req.body;

        const user = await User.findOne({ email: email });

        if (!user) return res.status(400).json({ message: "User doesnt exist" , Status:"Declined"});

        const isValid = await comparePass(password, user.password);

        if (isValid) {
            const token = jwt.sign({ email: user.email, role: user.role }, "secret-key-mernAuth", { expiresIn: "1d" });
            res.cookie('token', token);
            return res.json({ Status: "Success", role: user.role })
        } else {
            return res.json("The password is incorrect")
        }

    } catch (error) {
        res.status(500).json(error, 'error while logging in the user');
    }
}


export const verifyAdmin = async(req,res)=>{
    try {
        res.status(200).json({Status:"Success"})
    } catch (error) {
        res.status(500).json(error, "error while verifying admin");
    }
}