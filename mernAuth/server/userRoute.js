import { Router } from "express";
import jwt from 'jsonwebtoken'
import { loginUser, registerUser, verifyAdmin } from "./userController.js";


const router = Router();


const verify = (req, res, next) => {
    const token = req.cookies.token;
    if (!token) {
        return res.status(400).json(" token is missing");
    } else {
        jwt.verify(token, "secret-key-mernAuth",
            (err, decoded) => {
                if (err) {
                    return res.json("error with token", err)
                } else {
                    if (decoded.role === "admin") {
                        next()
                    } else {
                        return res.json("not admin")
                    }
                }
            });
    }
}

router.post("/register", registerUser);
router.post("/login", loginUser);
router.get("/admin", verify, verifyAdmin)

export default router;