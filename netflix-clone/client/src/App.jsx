import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import VideoPlayer from './components/VideoPlayer'
import TVshows from './pages/TVShows';
import { Home,Login, Movies, Signup, UserLikedMovies } from './pages';


function App() {

  return (
    <Router>
      <Routes>
        <Route path='/' exact element={<Login />} />
        <Route path='/signup' exact element={<Signup />} />
        <Route path='/home' exact element={<Home />} />
        <Route path='/video' exact element={<VideoPlayer />} />
        <Route path='/movies' exact element={<Movies />} />
        <Route path='/tv' exact element={<TVshows />} />
        <Route path='/mylist' exact element={<UserLikedMovies />} />
      </Routes>
    </Router>
  )
}

export default App;