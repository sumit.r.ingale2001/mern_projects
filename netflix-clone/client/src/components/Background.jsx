import { styled } from '@mui/material'
import bg from '../assets/login.jpg'

const Image = styled("img")`
height:100vh;
width:100vw;
object-fit:cover;
object-position:center;
filter:brightness(50%);
`

const Background = () => {
    return (
        <>
            <Image src={bg} alt="bg image" />
        </>
    )
}

export default Background;
