/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
/* eslint-disable react-refresh/only-export-components */
import React, { useEffect, useState } from "react"
import { Box } from '@mui/material'
import { useNavigate } from "react-router-dom";
import styled from "@emotion/styled";
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import ThumbUpIcon from '@mui/icons-material/ThumbUp';
import ThumbDownIcon from '@mui/icons-material/ThumbDown';
import CheckIcon from '@mui/icons-material/Check';
import AddIcon from '@mui/icons-material/Add';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import { onAuthStateChanged } from "firebase/auth";
import { firebaseAuth } from "../utils/firebase-config";
import axios from 'axios'
import { useDispatch, useSelector } from "react-redux";
import {
    getTrailers,
    getVideo,
    removeFromLikedMovies
} from "../store";
import Youtube from 'react-youtube'
import DemoVideo from '../assets/StrangerThings.mp4'

const Container = styled(Box)`
max-width:280px;
width:280px;
height:100%;
cursor:pointer;
position:relative;
& >img {
    border-radius:0.2rem;
    width:100%;
    height:100%;
    z-index:10;
}
`

const Hover = styled(Box)`
z-index:20;
height:max-content;
position:absolute;
width:20rem;
top:-18vh;
left:0;
border-radius:0.3rem;
box-shadow:rgba(0,0,0,0.8) 0px 3px 10px;
background:#181818;
transition:0.3s ease-in-out;
`

const ImageVideoContainer = styled(Box)`
position:relative;
height:140px;
img{
    width:100%;
    height:140px;
    object-fit:cover;
    border-radius:0.3rem;
    top:0;
    z-index:5,
    position:absolute;
};
video, iframe{
    width:100%;
    height:140px;
    object-fit:cover;
    border-radius:0.3rem;
    top:0;
    left:0;
    z-index:5;
    position:absolute;
};

`

const InfoContainer = styled(Box)`
display:flex;
flex-direction:column;
padding:1rem;
gap:0.5rem;
`

const Icons = styled(Box)`
display:flex;
justify-content:space-between;
& > div{
    display:flex;
    gap:1rem;
}
svg{
    font-size:2rem;
    cursor:pointer;
    transition:0.3s ease-in-out;
    :hover{
        color:#b8b8b8;
    }
}
`
const Info = styled(Box)`

`
const Genres = styled(Box)`
display:flex;
& > ul{
    display:flex;
    gap:1rem;
    & > li {
        padding-right:0.7rem;
        &:first-of-type{
            list-style-type:none;
        }
    }
}
`

export default React.memo(function Card({ movieData, isLiked = false }) {

    const [isHovered, setIsHovered] = useState(false);
    const navigate = useNavigate();
    const [email, setEmail] = useState(undefined)
    const dispatch = useDispatch();
    const [videoName, setVideoName] = useState('');
    const trailers = useSelector((store) => store.netflix.trailers)

    // useEffect(() => {
    //     video()
    // },[])

    // const video = async () => {
    //     const data = await getVideo(movieData.id)
    //     let url = data.videos.results.find((vid) => vid.name === 'Official Trailer');
    //     setVideoName(url?.key ? url.key : null)
    // }

    // useEffect(() => {
    //     video()
    // }, [])

    // const video = () => {
    //     dispatch(getTrailers(movieData.id))
    //     let url = trailers.videos.results.find((vid) => vid.name === "Official Trailer")
    //     setVideoName(url.key)
    // }

    useEffect(() => {
        dispatch(getTrailers(movieData.id));
    }, [])

    console.log(trailers)





    onAuthStateChanged(firebaseAuth, (currentUser) => {
        if (currentUser) setEmail(currentUser.email);
        else navigate("/")
    });

    const addToList = async () => {
        try {
            await axios.post("http://localhost:8000/api/user/add",
                { email, data: movieData })
        } catch (error) {
            console.log(error);
        }
    }
    return (
        <Container
            onMouseEnter={() => setIsHovered(true)}
            onMouseLeave={() => setIsHovered(false)}
        >
            <img src={`https://image.tmdb.org/t/p/w500${movieData.image}`} alt="" />
            {
                isHovered && (
                    <Hover>
                        <ImageVideoContainer>
                            <img src={`https://image.tmdb.org/t/p/w500${movieData.image}`} alt=""
                                onClick={() => navigate('/video')}
                            />
                            {
                                !videoName ? (
                                    <video src={DemoVideo}
                                        onClick={() => navigate('/video')}
                                        autoPlay />
                                ) : (
                                    <Youtube
                                        videoId={videoName}
                                    />
                                )
                            }
                        </ImageVideoContainer>
                        <InfoContainer>
                            <h3 >{movieData.name}</h3>
                            <Icons>
                                <Box>
                                    <PlayArrowIcon
                                        onClick={() => navigate('/video')}
                                        title='play'
                                    />
                                    <ThumbUpIcon title='like' />
                                    <ThumbDownIcon title='dislike' />
                                    {
                                        isLiked ? (
                                            <CheckIcon title='remove from list'
                                                onClick={() => dispatch(removeFromLikedMovies({ movieId: movieData.id, email }))}
                                            />
                                        ) : (
                                            <AddIcon title='add to my list' onClick={addToList} />
                                        )
                                    }
                                </Box>
                                <Info>
                                    <KeyboardArrowDownIcon title="More info" />
                                </Info>
                            </Icons>
                            <Genres>
                                <ul>
                                    {movieData.genres.map((genre) => (
                                        <li key={genre}>{genre}</li>
                                    ))}
                                </ul>
                            </Genres>
                        </InfoContainer>
                    </Hover>
                )
            }
        </Container>
    )
})
