/* eslint-disable react-refresh/only-export-components */
/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */

import Card from "./Card";
import React, { useRef, useState } from "react";
import { styled, Box } from '@mui/material'
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';

const Container = styled(Box)`
display:flex;
flex-direction:column;
position:relative;
padding:1rem 0;
gap:1rem;
h1{
    margin-left:50px;
}
& > div{
    display:flex;
}
`
const Wrapper = styled(Box)`
`
const Slider = styled(Box)`
display:flex;
justify-content:center;
align-items:center;
width:max-content;
gap:1rem;
transform:translateX(0px);
transition:0.3s ease-in-out;
margin-left:10px;
`
const Right = styled(Box)`
position:absolute;
z-index:99;
height:100%;
display:flex;
justify-content:center;
align-items:center;
width:50px;
right:0;
bottom:0;
height:80%;
cursor:pointer;
transition:0.3s ease-in-out;
svg{
    font-size:3rem;
}
`
const Left = styled(Box)`
position:absolute;
cursor:pointer;
z-index:99;
height:80%;
display:flex;
justify-content:center;
align-items:center;
bottom:0;
width:50px;
left:0;
transition:0.3s ease-in-out;
svg{
    font-size:3rem;
}
`

export default React.memo(function Cardslider({ data, title }) {

    const listRef = useRef();


    const [showControls, setshowControls] = useState(false);
    const [sliderPosition, setSliderPosition] = useState(0);


    const handleDirection = (direction) => {
        let distance = listRef.current.getBoundingClientRect().x - 70;
        if (direction === 'left' && sliderPosition > 0) {
            listRef.current.style.transform = `translateX(${230 + distance}px)`
            setSliderPosition(sliderPosition - 1)
        }
        if (direction === 'right' && sliderPosition < 4) {
            listRef.current.style.transform = `translateX(${-230 + distance}px)`
            setSliderPosition(sliderPosition + 1)
        }
    }

    return (
        <Container
            onMouseEnter={() => setshowControls(true)}
            onMouseLeave={() => setshowControls(false)}
        >
            <h1>{title}</h1>
            <Wrapper>
                <Left
                    sx={{ display: !showControls ? "none" : '' }}
                >
                    <ArrowBackIosNewIcon onClick={() => handleDirection('left')} />
                </Left>
                <Slider
                    ref={listRef}
                >
                    {
                        data.map((movie, index) => (
                            <Box key={movie.id}>
                                <Card index={index} movieData={movie} title={title} />
                            </Box>
                        ))
                    }
                </Slider>
                <Right
                    sx={{ display: !showControls ? "none" : '' }}
                >
                    <ArrowForwardIosIcon
                        onClick={() => handleDirection('right')} />
                </Right>
            </Wrapper>
        </Container>
    )
})
