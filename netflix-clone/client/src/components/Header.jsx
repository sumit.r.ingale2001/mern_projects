/* eslint-disable react/prop-types */
import { styled, Box, AppBar, Toolbar, Button } from "@mui/material"
import logo from '../assets/logo.png'
import { useNavigate } from "react-router-dom"

const StyledToolbar = styled(Toolbar)`
display:flex;
align-items:center;
justify-content:space-between;
`

const Header = ({ login }) => {
    const navigate = useNavigate();
    return (
        <AppBar
            sx={{ background: "none", boxShadow: "none" }}
        >
            <StyledToolbar>
                <Box>
                    <img src={logo} alt="netflix logo" width={150} />
                </Box>

                <Button variant="contained" color="error" size="medium"
                    onClick={() => navigate(login ? '/' : "/signup")}
                >
                    {login ? "Log in" : "Sign up"}
                </Button>
            </StyledToolbar>
        </AppBar>
    )
}

export default Header
