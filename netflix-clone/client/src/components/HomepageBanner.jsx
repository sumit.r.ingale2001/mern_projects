import { styled } from '@mui/material'
import Banner from '../assets/home.jpg'

const Image = styled("img")`
height:100%;
width:100%;
object-fit: cover;
object-position:center;
filter:brightness(30%);
position:absolute;
top:0;
left:0;
z-index:-1;
`
const HomepageBanner = () => {
    return (
        <>
            <Image src={Banner} alt="homepage banner" />
        </>
    )
}

export default HomepageBanner
