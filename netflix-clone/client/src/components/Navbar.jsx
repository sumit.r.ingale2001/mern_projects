/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */

import { styled, Box, Button } from '@mui/material'
import logo from '../assets/logo.png'
import { Link, useNavigate } from 'react-router-dom';
import { useState } from 'react';
import { Menu, Close, PowerSettingsNew } from '@mui/icons-material';
import { onAuthStateChanged, signOut } from 'firebase/auth';
import { firebaseAuth } from '../utils/firebase-config';



const NavbarContainer = styled(Box)(({ theme }) => ({
    padding: "1rem 1.5rem",
    display: "flex",
    alignItems: "center",
    position: "fixed",
    zIndex: 2,
    top: 0,
    left: 0,
    width: "100vw",
    color: "white",
    justifyContent: "space-between",
    overflow: "hidden",
    [theme.breakpoints.down("lg")]: {
        display: "block",
    }
}));

const Container = styled(Box)(({ theme }) => ({
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",

}))

const StyledUl = styled("ul")(({ theme }) => ({
    listStyle: "none",
    display: "flex",
    alignItems: "center",
    "& > li": {
        margin: "0 10px",
        color: "white"
    },
    "& a": {
        textDecoration: "none",
        color: "white"
    },
    [theme.breakpoints.down("lg")]: {
        flexDirection: "column",
        marginTop: "15px",
        alignItems: "start",
        "& > li": {
            margin: "10px"
        }
    }
}))

// links array for the navbar links 
const links = [
    { name: "Home", link: "/" },
    { name: "TV Shows", link: "/tv" },
    { name: "Movies", link: "/movies" },
    { name: "My List", link: "/mylist" },
];


const Navbar = ({ isScrolled }) => {
    const navigate = useNavigate()
    const [showMenu, setShowMenu] = useState(false);

    onAuthStateChanged(firebaseAuth, (currentUser) => {
        if (!currentUser) navigate('/')
    })
    return (
        <NavbarContainer
            sx={{
                height: showMenu ? "auto" : "6.5rem",
                background: isScrolled || showMenu ? "black" : ""
            }}
        >
            <Container>
                <Link to='/home' >
                    <img src={logo} alt="netflix logo" width={150} />
                </Link>
                <Button
                    variant='contained'
                    color="error"
                    size='small'
                    sx={{ display: { md: "block", lg: "none !important" } }}
                    onClick={() => setShowMenu(!showMenu)}
                >
                    {
                        showMenu ? <Close /> : <Menu />
                    }
                </Button>
            </Container>
            <StyledUl>
                {
                    links.map((item) => {
                        const { name, link } = item;
                        return (
                            <li key={name} >
                                <Link to={link} >{name}</Link>
                            </li>
                        )
                    })
                }
                <PowerSettingsNew
                    sx={{ ml: "10px", cursor: "pointer", ":hover": { color: "darkred" } }}
                    onClick={() => signOut(firebaseAuth)}
                    fontSize='large'
                    color='error'
                />
            </StyledUl>
        </NavbarContainer >
    )
}

export default Navbar
