/* eslint-disable react/prop-types */

const NotAvailable = ({text}) => {
    return (
        <h1
            style={{ textAlign: "center", color: "white", margin:"3rem" }}
        >
            {text}
        </h1>
    )
}

export default NotAvailable;