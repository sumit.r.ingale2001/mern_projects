/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import { styled } from '@mui/material'
import { useDispatch } from 'react-redux'
import { fetchMoviesByGenre } from '../store';

const Select = styled("select")`
margin-left:1rem;
padding:10px 20px;
font-size:1.1rem;
background:none;
color:white;
& > option{
    background:black;
}
`

const SelectGenre = ({ genres, type }) => {
    const dispatch = useDispatch();
    return (

        <Select
            onChange={(e) => dispatch(fetchMoviesByGenre({ genre: e.target.value, type }))}
            labelId="demo-simple-select-label"
            id="demo-simple-select"
        >
            {
                genres.map((genre, index) => (
                    <option value={genre.id} key={genre.id} >{genre.name}</option>
                ))
            }
        </Select>
    )
}

export default SelectGenre
