/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */

import Cardslider from "./Cardslider"
import { Box, styled } from '@mui/material'

const Container = styled(Box)`
width:100vw;
`

const Slider = ({ movies }) => {
    const getMoviesFromRange = (from, to) => {
        return movies.slice(from, to)
    }
    
    return (
        <Container>
            <Cardslider title="Trending Now" data={getMoviesFromRange(0, 10)} />
            <Cardslider title="New Releases" data={getMoviesFromRange(10, 20)} />
            <Cardslider title="Blockbuster Movies" data={getMoviesFromRange(20, 30)} />
            <Cardslider title="Popular on Netflix" data={getMoviesFromRange(30, 40)} />
            <Cardslider title="Action Movies" data={getMoviesFromRange(40, 50)} />
            <Cardslider title="Epics" data={getMoviesFromRange(50, 60)} />
        </Container>
    )
}

export default Slider


