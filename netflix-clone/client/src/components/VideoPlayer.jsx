/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import video from '../assets/StrangerThings.mp4';
import { styled } from '@mui/material';

const Video = styled("video")(({ theme }) => ({
  height: "100vh",
  width: "100vw",
  objectFit: "contain",
}))

const VideoPlayer = () => {
  return (
    <>
      <Video src={video} autoPlay muted controls />
    </>
  )
}

export default VideoPlayer
