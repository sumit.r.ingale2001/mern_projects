/* eslint-disable no-unused-vars */
import { useState } from "react"
import { Box, Button } from "@mui/material";
import { HomepageBanner, Navbar } from '../components/index'
import styled from "@emotion/styled";
import homeTitle from '../assets/homeTitle.webp'
import { PlayArrow, InfoOutlined } from '@mui/icons-material';
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchMovies, getGenres } from "../store";
import Slider from "../components/Slider";

const HomeContainer = styled(Box)`
position:relative;
height:100vh;
width:100vw;
`

const Image = styled("img")(({ theme }) => ({
    width: "40rem"
}))

const ButtonContainer = styled(Box)(({ theme }) => ({
    display: "flex",
    alignItems: "center",
    marginTop: 5,
    "& > button": {
        margin: "0 5px"
    }
}))

const Home = () => {
    const [isScrolled, setIsScrolled] = useState(false);
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const genresLoaded = useSelector((state) => state.netflix.genresLoaded);
    const movies = useSelector((state) => state.netflix.movies)

    useEffect(() => {
        dispatch(getGenres())
    }, [])

    useEffect(() => {
        if (genresLoaded) dispatch(fetchMovies({ type: "all" }))
    }, [genresLoaded])



    window.onscroll = () => {
        setIsScrolled(window.pageYOffset === 0 ? false : true);
        return () => window.onscroll = null
    }
    return (
        <>
            <Navbar isScrolled={isScrolled} />
            <HomeContainer>
                <HomepageBanner />
                <Box
                    sx={{
                        position: "absolute",
                        bottom: "150px",
                        left: "40px"
                    }}
                >
                    <Image src={homeTitle} alt="homepage title" />
                    <ButtonContainer>
                        <Button
                            onClick={() => navigate('/video')}
                            variant="contained"
                            color="inherit"
                            size="large"
                            sx={{ color: "black" }}
                        >
                            <PlayArrow />
                            Play
                        </Button>
                        <Button
                            size="large"
                            variant="contained"
                            color="error"
                            sx={{ background: "rgba(225,225,225,0.5)" }}
                        >
                            <InfoOutlined />
                            info
                        </Button>
                    </ButtonContainer>
                </Box>
            </HomeContainer>
            <Slider movies={movies} />
        </>
    )
}

export default Home
