import { Box, Button, TextField, Typography, styled } from "@mui/material"
import { useState } from "react"
import VisibilityIcon from '@mui/icons-material/Visibility';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import { useNavigate } from 'react-router-dom'
import {Header, Background} from '../components/index';
import { onAuthStateChanged, signInWithEmailAndPassword } from "firebase/auth";
import { firebaseAuth } from "../utils/firebase-config";

const Container = styled(Box)`
overflow:hidden;
width:100vw;
height: 100vh;
position:relative;
display:flex;
justify-content:center;
align-items:center;
`

const Form = styled("form")`
background: rgba(0,0,0,0.8);
padding:15px 50px;
display:flex;
align-items:center;
flex-direction:column;
& input{
    padding:8px 15px;
    background:none;
    height:100%;
    width:100%;
}

& > h1{
    color:white;
    text-align:center;
}

& > div{
    width:100%;
    background:rgba(225,225,225,0.5);
    margin:10px 0;
    display:flex;
    align-items:center;
    justify-content:space-between;
    padding:0 5px;
}
`

const Login = () => {

    const navigate = useNavigate();
    const [showPassword, setShowPassword] = useState(false);
    const [formValues, setFormValues] = useState(
        {
            email: "",
            password: ""
        }
    )

    const handleSubmit =async (e) => {
        e.preventDefault()
        try {
            const {email,password} = formValues;
            await signInWithEmailAndPassword(firebaseAuth, email,password)
        } catch (error) {
            console.log(error, "error while signing in the user")
        }
    }

    onAuthStateChanged(firebaseAuth,(currentUser)=>{
        if(currentUser) navigate('/home')
    })

    return (
        <Container>
            <Header />
            <Box
                sx={{
                    position: "absolute",
                    top: 0,
                    left: 0,
                    zIndex: -1
                }}
            >
                <Background />
            </Box>
            <Form>
                <h1>Log in</h1>
                <Box>
                    <TextField
                        type="email"
                        name="email"
                        onChange={(e) => setFormValues({ ...formValues, [e.target.name]: e.target.value })}
                        value={formValues.email}
                        label="Enter your email"
                        variant="standard"
                        sx={{ width: "25vw" }}
                    />
                </Box>
                <Box>
                    <TextField
                        type={`${showPassword ? "text" : "password"}`}
                        value={formValues.password}
                        onChange={(e) => setFormValues({ ...formValues, [e.target.name]: e.target.value })}
                        name="password"
                        label="Enter your password"
                        variant="standard"
                        sx={{ width: "25vw" }} />
                    <Box
                        sx={{ cursor: "pointer" }}
                        onClick={() => setShowPassword(!showPassword)}
                    >
                        {showPassword ? <VisibilityOffIcon /> : <VisibilityIcon />}
                    </Box>
                </Box>
                <Button size="small"
                    fullWidth
                    variant="contained"
                    color='error'
                    onClick={handleSubmit}
                    sx={{ m: "10px 0", p: "10px" }}
                >Log in</Button>

                <Typography
                    sx={{ color: "white" }}
                    fontSize={14}
                >
                    Not a member ?&nbsp;
                    <Box onClick={() => navigate('/signup')} sx={{ cursor: "pointer", color: "red" }} component='span'>Sign up</Box>
                </Typography>
            </Form>
        </Container >
    )
}

export default Login
