import { Box, Button, styled } from "@mui/material"
import { Background, Header } from '../components/index'
import { useState } from "react";
import { createUserWithEmailAndPassword, onAuthStateChanged } from 'firebase/auth'
import { firebaseAuth } from '../utils/firebase-config'
import { useNavigate } from "react-router-dom";
import VisibilityOutlinedIcon from '@mui/icons-material/VisibilityOutlined';
import VisibilityOffOutlinedIcon from '@mui/icons-material/VisibilityOffOutlined';

const MainContainer = styled(Box)`
position:relative;
display:flex;
justify-content:center;
align-items:center;
height:100vh;
width:100vw;
color:white;
text-align:center;
padding:10px 20px;
overflow:hidden;
`
const Container = styled(Box)`
text-align:center;
display:flex;
align-items:center;
flex-direction:column;
& > div{
    gap:1rem;
    margin:10px 0;
}`

const Form = styled("form")`
display:grid;
width:70%;
& > div{
display:flex;
align-items:center;
color:black;
background:white;
border:1px solid black;
}
& input{
    padding:1.5rem;
    outline:none;
    width:100%;
    border:none;
    font-size:1.2rem;
}
`;

const Signup = () => {
    const navigate = useNavigate();
    const [showPassword, setShowPassword] = useState(false)
    const [showPasswordBtn, setShowPasswordBtn] = useState(false)
    const [formValues, setFormValues] = useState({
        email: "",
        password: ""
    })

    const handleSignup = async (e) => {
        e.preventDefault();
        try {
            const { email, password } = formValues;
            await createUserWithEmailAndPassword(firebaseAuth, email, password);
        } catch (error) {
            console.log(error, "error while handling signup")
        }
    }

    onAuthStateChanged(firebaseAuth, (currentUser) => {
        if (currentUser) navigate('/home')
    })

    return (
        <MainContainer>
            <Header login />
            <Box
                sx={{
                    position: "absolute",
                    top: 0,
                    left: 0,
                    zIndex: -1
                }}
            >
                <Background />
            </Box>
            <Container>
                <Box
                    sx={{
                        display: "flex",
                        flexDirection: "column",
                        fontSize: { sm: "1.5rem", lg: "2rem" }
                    }}
                >
                    <h1
                    >Unlimited movies, TV shows and more</h1>
                    <h4>Watch anywhere. Cancel anytime.</h4>
                    <h6>
                        Ready to watch? Enter your email to create or restart membership
                    </h6>
                </Box>
                <Form
                    sx={{ gridTemplateColumns: `${showPasswordBtn ? "1fr 1fr" : "2fr 1fr"}` }}
                >
                    <Box>
                        <input type="email"
                            style={{ border: "1px solid black" }}
                            name="email"
                            placeholder="Enter your email"
                            value={formValues.email}
                            onChange={(e) => setFormValues({ ...formValues, [e.target.name]: e.target.value })}
                        />
                    </Box>
                    {
                        showPasswordBtn && (
                            <Box
                            >
                                <input type={showPassword ? "text" : "password"}
                                    name="password"
                                    placeholder="Enter your password"
                                    value={formValues.password}
                                    onChange={(e) => setFormValues({ ...formValues, [e.target.name]: e.target.value })}
                                />
                                <span
                                    style={{ marginRight: "5px", cursor: "pointer" }}
                                    onClick={() => setShowPassword(!showPassword)}
                                >{showPassword ? <VisibilityOffOutlinedIcon /> : <VisibilityOutlinedIcon />}</span>
                            </Box>
                        )
                    }

                    {
                        !showPasswordBtn && <Button
                            variant="contained"
                            color="error"
                            fullWidth
                            onClick={() => setShowPasswordBtn(true)}
                        >Get Started</Button>
                    }
                </Form>
                <Button
                    sx={{ my: "10px" }}
                    color="error"
                    variant="contained"
                    onClick={() => handleSignup()}
                >
                    Sign up
                </Button>
            </Container>
        </MainContainer >
    )
}

export default Signup
