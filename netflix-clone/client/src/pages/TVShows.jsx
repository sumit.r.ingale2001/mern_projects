/* eslint-disable no-unused-vars */
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { fetchMovies, getGenres } from "../store";
import { onAuthStateChanged } from "firebase/auth";
import { firebaseAuth } from "../utils/firebase-config";
import { Box, styled } from "@mui/material";
import {SelectGenre,NotAvailable, Slider, Navbar} from "../components/index";


const Data = styled(Box)`
margin-top:8rem;
`

const TVshows = () => {
    const [isScrolled, setIsScrolled] = useState(false);
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const [user, setUser] = useState(undefined)


    window.onscroll = () => {
        setIsScrolled(window.pageYOffset === 0 ? false : true);
        return () => window.onscroll = null
    }

    const genresLoaded = useSelector((state) => state.netflix.genresLoaded);
    const movies = useSelector((state) => state.netflix.movies);
    const genres = useSelector((state) => state.netflix.genres)


    useEffect(() => {
        dispatch(getGenres())
    }, []);

    onAuthStateChanged(firebaseAuth, (currentUser) => {
        if (currentUser) setUser(currentUser.uid);
        else navigate("/")
    })


    useEffect(() => {
        if (genresLoaded) dispatch(fetchMovies({ genres, type: "tv" }))
    }, [genresLoaded])

    return (
        <Box>
            <Navbar isScrolled={isScrolled} />
            <Data>
                <SelectGenre genres={genres} type='tv' />
                {
                    movies.length ? <Slider movies={movies} /> : <NotAvailable text='No movies available for selected genre' />
                }
            </Data>
        </Box>
    )
}

export default TVshows;
