import { useDispatch, useSelector } from "react-redux";
import { getUserLikedMovies } from "../store";
import { useEffect, useState } from "react";
import { firebaseAuth } from "../utils/firebase-config";
import { onAuthStateChanged } from "firebase/auth";
import { useNavigate } from "react-router-dom";
import { Box, styled } from "@mui/material";
import Navbar from "../components/Navbar";
import Card from "../components/Card";
import NotAvailable from "../components/NotAvailable";


const Content = styled(Box)`
dispaly:flex;
flex-direction:column;
margin:2.3rem;
margin-top:8rem;
gap:3rem;
h1{
    margin:1rem 0 1rem 3rem;
}
`
const MoviesContainer = styled(Box)`
flex-wrap:wrap;
gap:1rem;
display:flex;
`

const UserLikedMovies = () => {
    const [isScrolled, setIsScrolled] = useState(false);
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const movies = useSelector((state) => state.netflix.movies);
    const [email, setEmail] = useState(undefined);


    window.onscroll = () => {
        setIsScrolled(window.pageYOffset === 0 ? false : true);
        return () => window.onscroll = null
    }
    onAuthStateChanged(firebaseAuth, (currentUser) => {
        if (currentUser) setEmail(currentUser.email);
        else navigate("/")
    });



    useEffect(() => {
        if (email) {
            dispatch(getUserLikedMovies(email))
        }
    }, [email]);


    return (
        <Box>
            <Navbar isScrolled={isScrolled} />
            {
                movies?.length <= 0 ? (
                    <NotAvailable text='No movies added to my list'/>
                ) : (
                    <Content>
                        <h1>My List</h1>
                        <MoviesContainer>
                            {
                                movies?.map((movie, index) => {
                                    return <Card movieData={movie} index={index} key={movie.id} isLiked={true} />
                                })
                            }
                        </MoviesContainer>
                    </Content>
                )
            }
        </Box>
    )
}

export default UserLikedMovies
