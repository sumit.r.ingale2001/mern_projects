/* eslint-disable no-unused-vars */
import { configureStore, createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { API_KEY, TMDB_BASE_URL } from '../utils/constants';
import axios from 'axios'


const initialState = {
    movies: [],
    genresLoaded: false,
    genres: [],
    trailers: [],
}


export const getVideo = async (id) => {
    const { data } = await axios.get(`${TMDB_BASE_URL}/movie/${id}?api_key=${API_KEY}&append_to_response=videos`);
    return data;
}

export const getTrailers = createAsyncThunk("netflix/trailers", async (id) => {
    try {
        const { data: { videos: { results } } } = await axios.get(`${TMDB_BASE_URL}/movie/${id}?api_key=${API_KEY}&append_to_response=videos`);
        return results;
    } catch (error) {
        console.log(error, "error while calling getTrailers api")
    }
})
export const getGenres = createAsyncThunk("netflix/genres", async () => {
    try {
        const { data: { genres } } = await axios.get(`${TMDB_BASE_URL}/genre/movie/list?api_key=${API_KEY}`)
        return genres;
    } catch (error) {
        console.log(error, "error while calling getGenres api");
    }
});

const createArrayFromRawData = (array, movieArray, genres) => {
    array.forEach((movie) => {
        const movieGenres = [];
        movie.genre_ids.forEach((genre) => {
            const name = genres.find(({ id }) => id === genre);
            if (name) movieGenres.push(name.name)
        });
        if (movie.backdrop_path) {
            movieArray.push({
                id: movie.id,
                name: movie?.original_name ? movie.original_name : movie.original_title,
                image: movie.backdrop_path,
                genres: movieGenres.slice(0, 3),
                videos: movie?.video,
            })

        }
    })
}

const getRawData = async (api, genres, paging) => {
    try {
        const moviesArray = [];
        for (let i = 1; i < 10 && moviesArray.length < 60; i++) {
            const { data: { results } } = await axios.get(`${api}${paging ? `&page=${i}` : ""}`)
            createArrayFromRawData(results, moviesArray, genres)
        }
        return moviesArray;
    } catch (error) {
        console.log(error, "error while calling getRawData api");
    }
}

export const fetchMovies = createAsyncThunk('netflix/trending', async ({ type }, thunkApi) => {
    try {
        const { netflix: { genres } } = thunkApi.getState();
        return getRawData(`${TMDB_BASE_URL}/trending/${type}/week?api_key=${API_KEY}`, genres, true);

    } catch (error) {
        console.log(error, 'error while calling fetchMovies api')
    }
})


export const fetchMoviesByGenre = createAsyncThunk('netflix/genre', async ({ genre, type }, thunkApi) => {
    try {
        const { netflix: { genres } } = thunkApi.getState();
        return getRawData(`${TMDB_BASE_URL}/discover/${type}/?api_key=${API_KEY}&with_genres=${genre}`, genres);

    } catch (error) {
        console.log(error, 'error while calling fetchMovies api')
    }
})

export const getUserLikedMovies = createAsyncThunk("netflix/getLiked", async (email) => {
    try {
        const { data: { movies } } = await axios.get(`http://localhost:8000/api/user/liked/${email}`);
        return movies;
    } catch (error) {
        console.log("error while getting the user liked movies", error)
    }
})

export const removeFromLikedMovies = createAsyncThunk("netflix/removeLiked", async ({ movieId, email }) => {
    try {
        const { data: { movies } } = await axios.put('http://localhost:8000/api/user/remove', { email, movieId });
        return movies;
    } catch (error) {
        console.log(error, 'error while calling remove from liked movies api')
    }
})


const NetflixSlice = createSlice({
    name: "netflix",
    initialState,
    extraReducers: (builders) => {
        builders.addCase(getGenres.fulfilled, (state, action) => {
            state.genres = action.payload;
            state.genresLoaded = true;
        });
        builders.addCase(fetchMovies.fulfilled, (state, action) => {
            state.movies = action.payload
        });
        builders.addCase(fetchMoviesByGenre.fulfilled, (state, action) => {
            state.movies = action.payload
        });
        builders.addCase(getUserLikedMovies.fulfilled, (state, action) => {
            state.movies = action.payload
        });
        builders.addCase(removeFromLikedMovies.fulfilled, (state, action) => {
            state.movies = action.payload;
        });
        builders.addCase(getTrailers.fulfilled, (state, action) => {
            state.trailers = action.payload
        })
    }
});

export const store = configureStore(
    {
        reducer: {
            netflix: NetflixSlice.reducer,
        }
    }

)