import User from "../models/userSchema.js";

export const addToLikedMovies = async (req, res) => {
    try {
        const { email, data } = req.body;
        const user = await User.findOne({ email });
        if (user) {
            const { likedMovies } = user;
            const movieAlreadyLiked = likedMovies.find(({ id }) => id === data.id)
            if (!movieAlreadyLiked) {
                await User.findByIdAndUpdate(user._id, {
                    likedMovies: [...user.likedMovies, data],
                },
                    { new: true }
                )
            } else {
                res.status(500).json({ message: "movie already liked " })
            }
        }
        else {
            await User.create({ email, likedMovies: [data] })
        }
        return res.json({ message: "movie added" })
    } catch (error) {
        res.status(500).json(error, "error while adding movie")
    }
}

export const getLikedMovies = async (req, res) => {
    try {
        const { email } = req.params;
        const user = await User.findOne({ email });
        if (user) {
            res.status(200).json({ movies: user.likedMovies })
        } else {
            res.status(500).json(error, 'user with given email not found')
        }
    } catch (error) {
        res.status(500).json("error while getting the movies", error)
    }
}


export const removeFromLikedMovies = async (req, res) => {
    try {
        const { email, movieId } = req.body;
        const user = await User.findOne({ email });
        if (user) {
            const movies = user.likedMovies;
            const movieIndex = movies.findIndex(({ id }) => id === movieId);
            if (!movieIndex) {
                res.status(400).send({ msg: "Movie not found." });
            }
            movies.splice(movieIndex, 1);
            await User.findByIdAndUpdate(
                user._id,
                {
                    likedMovies: movies,
                },
                { new: true }
            );
            return res.json({ msg: "Movie successfully removed.", movies });
        } else return res.json({ msg: "User with given email not found." });
    } catch (error) {
        return res.json({ msg: "Error removing movie to the liked list" });
    }

}
