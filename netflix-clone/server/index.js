import express from 'express'
import cors from 'cors'
import Connection from './database/db.js';
import userRoutes from './routes/userRoutes.js'

const PORT = 8000;
const app = express();

app.use(cors());
app.use(express.json());
app.use('/api/user', userRoutes)


Connection();
app.listen(PORT, () => console.log(`server running on port ${PORT}`))
