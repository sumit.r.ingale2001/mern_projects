import axios from 'axios'

const url = 'http://localhost:8000';
axios.defaults.withCredentials = true;

export const signUpUser = async (data) => {
    try {
        return await axios.post(`${url}/auth/add`, data)
    } catch (error) {
        console.log(error, 'error while calling signUpUser api')
    }
}

export const loginUser = async (data) => {
    try {
        return await axios.post(`${url}/auth`, data)
    } catch (error) {
        console.log(error, "error while calling loginUser api")
    }
}

export const verifyUser = async () => {
    try {
        return await axios.get(`${url}/auth/verify`)
    } catch (error) {
        console.log(error, "error while calling verifyUser api");
    }
}