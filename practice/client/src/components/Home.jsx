/* eslint-disable no-unused-vars */
import { useEffect } from "react"
import { verifyUser } from "../api"
import { useCookies } from "react-cookie"
import { useNavigate } from "react-router-dom"
import { toast } from "react-toastify"

const Home = () => {

    const [cookies, setCookies, removeCookies] = useCookies(["access_token"])
    const navigate = useNavigate()

    useEffect(()=>{
        if(!cookies.access_token){
            navigate("/")
        }
    },[])

    useEffect(() => {
        const verify = async () => {
            try {
                const response = await verifyUser()
                console.log(response)
            } catch (error) {
                console.log(error, "error while verifying")
            }
        }
        verify()
    }, [])

    const logout = () => {
        localStorage.clear("userID")
        localStorage.clear("email")
        removeCookies("access_token");
        navigate("/login")
        toast.success("Logged out successfully")
    }
    return (
        <div className="d-flex gap-2" >
            <h1>home</h1>
            <button className="btn btn-primary" onClick={logout} >Logout</button>
        </div>
    )
}

export default Home
