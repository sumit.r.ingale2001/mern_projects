/* eslint-disable no-unused-vars */
import { useState } from "react"
import { Link, useNavigate } from "react-router-dom"
import { loginUser } from "../api"
import { toast } from "react-toastify"
import { useCookies } from 'react-cookie'

const Login = () => {

    const [showPassword, setShowPassword] = useState(false)
    const navigate = useNavigate()
    const [cookies, setCookies] = useCookies(["access_token"])
    const [formValues, setFormValues] = useState({
        email: "",
        password: ""
    })

    const handleChange = (e) => {
        setFormValues({ ...formValues, [e.target.name]: e.target.value })
    }

    const handleSumbit = async () => {
        const response = await loginUser(formValues)
        if (response) {
            if (response.data.error) {
                toast.error(response.data.error)
            } else if (response.data.warning) {
                toast.warn(response.data.warning);
            } else {
                toast.success(response.data.success)
                setCookies("access_token", response.data.token);
                localStorage.setItem("userID", response.data.userID);
                localStorage.setItem("email", response.data.email);
                navigate("/home")
            }
        } else {
            toast.error("Something went wrong")
        }
    }

    return (
        <div id="signup" className="d-flex justify-content-center align-items-center" >

            <div className="card px-3 py-2" style={{ maxWidth: "600px", width: "80%" }} >
                <h3 className="text-center" >Login</h3>
                <div className="card-body d-flex flex-column">

                    {/* for email  */}
                    <div className="input-group mb-3">
                        <input
                            type="email"
                            className="form-control"
                            placeholder="Email"
                            name="email"
                            onChange={handleChange}
                            aria-label="Email"
                            aria-describedby="basic-addon1" />
                    </div>
                    {/* for email  */}

                    {/* for password  */}
                    <div className="input-group mb-3">
                        <input
                            type={showPassword ? "text" : "password"}
                            className="form-control"
                            placeholder="Password"
                            name="password"
                            onChange={handleChange}
                            aria-label="Password"
                            aria-describedby="basic-addon1" />

                        <button className="btn btn-outline-primary" onClick={() => setShowPassword(!showPassword)} >{showPassword ? "Hide" : "Show"}</button>
                    </div>
                    {/* for password  */}

                    {/* submit button  */}
                    <button
                        className="btn btn-primary mb-3"
                        onClick={handleSumbit}
                    >
                        Login
                    </button>
                    {/* submit button  */}

                    {/* navigate to login  */}
                    <p>Not a member? <Link to="/" className="text-decoration-none" >Sign up</Link></p>
                    {/* navigate to login  */}
                </div>
            </div>

        </div>
    )
}

export default Login
