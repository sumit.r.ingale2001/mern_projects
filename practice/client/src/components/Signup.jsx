/* eslint-disable no-unused-vars */
import { useEffect, useState } from "react"
import { Link, useNavigate } from "react-router-dom"
import { signUpUser, verifyUser } from "../api"
import { toast } from "react-toastify"
import { useCookies } from "react-cookie"

const Signup = () => {

    const [cookies, setCookies] = useCookies(["access_token"]);
    const navigate = useNavigate()
    const [showPassword, setShowPassword] = useState(false)
    const [formValues, setFormValues] = useState({
        name: "",
        email: "",
        password: ""
    })

    const handleChange = (e) => {
        setFormValues({ ...formValues, [e.target.name]: e.target.value })
    }

    useEffect(() => {
        if (cookies.access_token) {
            navigate("/home")
        } else {
            navigate("/")
        }
    }, [])

    const handleSumbit = async () => {
        const response = await signUpUser(formValues)
        if (response) {
            if (response.data.error) {
                toast.error(response.data.error)
            } else if (response.data.warning) {
                toast.warn(response.data.warning)
            } else {
                toast.success(response.data.success);
                navigate("/login")
            }
        } else {
            toast.error("Something went wrong")
        }
    }

    return (
        <div id="signup" className="d-flex justify-content-center align-items-center" >

            <div className="card px-3 py-2" style={{ maxWidth: "600px", width: "80%" }} >
                <h3 className="text-center" >Sign up</h3>
                <div className="card-body d-flex flex-column">
                    {/* for name field  */}
                    <div className="input-group mb-3">
                        <input
                            type="text"
                            className="form-control"
                            placeholder="Name"
                            name="name"
                            onChange={handleChange}
                            aria-label="Name"
                            aria-describedby="basic-addon1" />
                    </div>
                    {/* for name field  */}

                    {/* for email  */}
                    <div className="input-group mb-3">
                        <input
                            type="email"
                            className="form-control"
                            placeholder="Email"
                            name="email"
                            onChange={handleChange}
                            aria-label="Email"
                            aria-describedby="basic-addon1" />
                    </div>
                    {/* for email  */}

                    {/* for password  */}
                    <div className="input-group mb-3 d-flex align-items-center">
                        <input
                            type={showPassword ? "text" : "password"}
                            className="form-control"
                            placeholder="Password"
                            name="password"
                            onChange={handleChange}
                            aria-label="Password"
                            aria-describedby="basic-addon1" />
                        <button className="btn btn-outline-primary " onClick={() => setShowPassword(!showPassword)}>{showPassword ? "Hide" : "Show"}</button>
                    </div>
                    {/* for password  */}


                    {/* submit button  */}
                    <button
                        className="btn btn-primary mb-3"
                        onClick={handleSumbit}
                    >
                        Sign up
                    </button>
                    {/* submit button  */}

                    {/* navigate to login  */}
                    <p>Already a member? <Link to="/login" className="text-decoration-none" >Login</Link></p>
                    {/* navigate to login  */}
                </div>
            </div>

        </div>
    )
}

export default Signup
