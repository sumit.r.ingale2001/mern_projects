import express from 'express';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import Connection from './db.js';
import userRoutes from './useRoutes.js'


const PORT = 8000;
const app = express();


app.use(express.json())
app.use(cors({
    origin: ["http://localhost:5173"],
    credentials: true,
    methods: ['GET', 'POST', 'PUT', 'DELETE']
}));
app.use(cookieParser());


app.use("/auth", userRoutes)

Connection()

app.listen(PORT, () => console.log(`server running on port ${PORT}`))
