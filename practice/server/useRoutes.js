import { Router } from "express";
import bcrypt from 'bcryptjs'
import User from "./userSchema.js";
import jwt from "jsonwebtoken"
import dotenv from 'dotenv'

dotenv.config()
const secret_Key = process.env.SECRET_KEY

const hashPass = async (pass) => {
    return await bcrypt.hash(pass, 10)
}

const comparePass = async (pass, databsePass) => {
    return await bcrypt.compare(pass, databsePass)
}

const router = Router();


// sign up user 
router.post("/add", async (req, res) => {
    try {
        const { name, password, email } = req.body;
        if (name.length < 1 || password.length < 1 || email.length < 1) {
            return res.json({ error: "Fields cannot be empty" })
        }

        const user = await User.findOne({ email })
        if (user) {
            return res.json({ error: "User already exist" })
        } else {
            if (!name) {
                return res.json({ error: "Name required" })
            }
            else if (!email) {
                return res.json({ error: "Email required" });
            }
            else if (!password || password.length < 6) {
                return res.json({ warning: "Password must be more than 5 characters" })
            }
            const hashedPassword = await hashPass(password);
            const newUser = new User({
                name, email, password: hashedPassword
            })
            await newUser.save();
            return res.json({ success: "Registered successfully" })
        }
    } catch (error) {
        res.status(500).json(error, "error while signing up the user")
    }
})


router.post("/", async (req, res) => {
    try {

        const { email, password } = req.body;
        if (password.length < 1 || email.length < 1) {
            return res.json({ error: "Fields cannot be empty" })
        }
        const user = await User.findOne({ email });

        if (!user) {
            return res.json({ error: "User does not exist" })
        } else {
            if (!email) {
                return res.json({ error: "Email required" })
            } else if (!password || password.length < 6) {
                return res.json({ warning: "Password must be more than 5 characters" })
            } else {
                const isValid = await comparePass(password, user.password)
                if (!isValid) {
                    return res.json({ error: "Password is incorrect" })
                } else {
                    const token = jwt.sign({ email: user.email, id: user._id }, secret_Key, { expiresIn: "5d" })
                    return res.json({ success: "Logged in successfully", token: token, userID: user._id, email: user.email })
                }
            }
        }

    } catch (error) {
        res.status(500).json(error, "error while logging in the user")
    }
})


const verify = async (req, res, next) => {
    try {
        const token = req.cookies.access_token;
        if (!token) {
            return res.json({ error: "Token missing" });
        } else {
            jwt.verify(token, secret_Key, (err) => {
                if (err) {
                    return res.json({ error: "Error in token" });
                } else {
                    next();
                }
            })
        }
    } catch (error) {
        res.json({ error: error.message, message: "Error while verifying the token" });
    }
}

router.get("/verify", verify, async (req, res) => {
    try {
        res.json({ success: "user Verified" })
    } catch (error) {
        res.status(500).json(error, "error  while verifying the user")
    }
})


export default router;