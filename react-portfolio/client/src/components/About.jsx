

const About = () => {
    return (
        <section id="about" className="p-3 mt-5">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <h2 className="text-center fw-bolder my-2">About</h2>
                    </div>
                </div>
                <div className="row justify-content-center p-5"
                    style={{ borderRadius: "10px 100px/ 120px", boxShadow: " 0 10px 20px rgba(0,0,0,0.3)" }}>
                    <div className="col-lg-12">
                        <p className="text-center fw-bolder fs-5 textColor">Hello, my name is Sumit Rajendra Ingale.</p>
                        <p className="fst-italic">I am from <strong className="textColor1">Ambernath, Thane, Mumbai</strong> I am a
                            Web developer, tool and die maker,
                            and machinist. Mostly calm and quiet guy, and a good listener. Among the projects
                            I have worked on are responsive websites like an Amazon clone, a portfolio website, a gaming
                            website, etc. <span className="fw-bolder" >PLEASE VISIT THE PROJECT SECTION FOR THE PROJECTS</span>
                        </p>
                    </div>
                </div>
                <div className="row my-3">
                    <div className="col-lg-12">
                        <h3 className="textColor text-center my-3 fw-semibold">Education Qualification</h3>
                    </div>
                </div>
                <div className="row justify-content-center align-items-center">
                    <div className="col-lg-4 qualification  d-flex flex-column justify-content-center align-items-center m-2"
                        style={{ borderRadius: 5, height: "16rem", width: ' 20rem' }}>
                        <h5 className="text-center fw-bold text-light">Diploma in Tool and Die Making, Idemi, Chunabhatti,
                            Mumbai</h5>
                        <h4 className="textColor1 fw-bold">2021</h4>
                        <div className="img diploma"></div>
                    </div>
                    <div className="col-lg-4 qualification  d-flex flex-column justify-content-center align-items-center  m-2"
                        style={{ borderRadius: 5, height: "16rem", width: '20rem' }}>
                        <h5 className="text-center fw-bold text-light">Machinist, ITI, Ambernath</h5>
                        <h4 className="textColor1 fw-bold">2018</h4>
                        <div className="img iti"></div>
                    </div>
                    <div className="col-lg-4 qualification  d-flex flex-column justify-content-center align-items-center  m-2"
                        style={{ borderRadius: 5, height: "16rem", width: ' 20rem' }}>
                        <h5 className="text-light text-center fw-bold">SSC</h5>
                        <h4 className="fw-bold textColor1">2016</h4>
                        <div className="img ssc"></div>
                    </div>
                </div>
            </div>
        </section >

    )
}

export default About
