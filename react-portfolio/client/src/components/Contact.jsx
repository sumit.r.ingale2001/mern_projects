import Socials from "./Socials"
import { useState } from 'react'
import { addUser } from "../service/api"
import { useNavigate } from 'react-router-dom'


const Contact = () => {

    const defaultValue = {
        name: "",
        email: "",
        phone: "",
        message: ""
    }

    const navigate = useNavigate();

    const [user, setUser] = useState(defaultValue);
    const [text, setText] = useState("")

    const handleChange = (e) => {
        setUser({ ...user, [e.target.name]: e.target.value });
    }

    const addUserDetails = async (e) => {
        e.preventDefault();
        await addUser(user);
        setText("Your query has been sent successfully")
        setTimeout(() => {
            navigate("/");
            setText("")
        }, 1500)
    }

    return (
        <section id="contact" className="p-5 mt-5">
            <div className="container">
                <div className="row">
                    <div className="col-lg-4  p-3">
                        <h1 className="fw-bolder contactme">Contact Me</h1>
                        <div className="fw-bold mt-5"><i className="bi bi-send-fill me-2 textColor1 fs-4"></i>
                            sumit.r.ingale2001@gmail.com</div>
                        <div className="fw-bold mt-3"><i className="bi bi-telephone-fill me-2 textColor1 fs-4"></i>7972741774</div>
                        <Socials />
                    </div>
                    <div className="col-lg-8 p-3">
                        <form>
                            <input type="text" name="name" className="form-control my-2" placeholder="Enter your name" required onChange={(e) => handleChange(e)} />
                            <input type="email" name="email" className="form-control my-2" placeholder="Enter your email" required onChange={(e) => handleChange(e)} />
                            <input type="number" name="phone" className="form-control my-2" placeholder="Enter your number" required onChange={(e) => handleChange(e)} />
                            <textarea name="message" className="form-control my-2 messageArea" cols="30" placeholder="Enter your message" rows="10" onChange={(e) => handleChange(e)} ></textarea>
                            <button onClick={(e) => addUserDetails(e)} className="btns mt-3 submit text-light">Submit</button>
                            <div className="p-1">
                                <p className="text-success" >{text}</p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Contact
