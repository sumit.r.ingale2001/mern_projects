
const Footer = () => {
    return (
        <footer className="p-3 box-shadow mt-2">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12 col-md-12 col-sm-12 my-2 d-flex justify-content-between align-items-center flex-wrap">
                        <h4 className="textColor fw-bolder">Sumit Ingale</h4>
                        <p> &copy;All rights reserved to SumitIngale </p>
                    </div>
                </div>
            </div>
        </footer>

    )
}

export default Footer
