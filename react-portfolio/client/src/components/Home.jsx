import profile_ from '../assets/profile_.webp'
import { useTypewriter, Cursor } from 'react-simple-typewriter'
import Skills from './Skills'
import About from './About'
import Socials from './Socials'
import Footer from './Footer'

const Home = () => {
    const [text] = useTypewriter({
        words: ["WEB DEVELOPER", "MECH ENGINEER", 'TOOL & DIE MAKER', 'MACHINIST'],
        loop: {}
    })
    return (
        <>
            <section id="home" className="d-flex justify-content-center align-items-center p-5 mt-3">
                <div className="container">
                    <div className="row mt-4">
                        <div className="col-lg-6 d-flex align-items-center justify-content-center p-4">
                            <div className="profile">
                                <img src={profile_} alt="" />
                            </div>
                        </div>
                        <div
                            className="col-lg-6 align-items-center d-flex align-items-center justify-content-center flex-column p-4">
                            <h1 className="fw-bolder text-center">Hi,</h1>
                            <h1 className="fw-bolder text-center h1class"><span className="textColor name" style={{ fontSize: "2em" }}>I am Sumit</span></h1>
                            <h3 className="textColor1 fw-bold typewriterText title mt-2">{text}<Cursor /></h3>
                            <p className="text-muted mt-2 text-center ">Good knowledge in web development, producing quality work</p>
                            <Socials/>
                        </div>
                    </div>
                </div>
            </section>
            <Skills />
            <About />
            <Footer/>
        </>

    )
}

export default Home
