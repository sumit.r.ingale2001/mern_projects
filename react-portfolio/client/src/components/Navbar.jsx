import { Link, NavLink } from "react-router-dom"
import '../App.css'
const Navbar = () => {
    return (
        <section id="nav" className="fixed-top box-shadow">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <div>
                            <nav className="navbar navbar-expand-lg">
                                <div className="container-fluid">
                                    <Link className="navbar-brand texthover textColor fw-bold fs-4" to='/' >Sumit Ingale</Link>
                                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                                        aria-label="Toggle navigation">
                                        <style>
                                            {
                                                `
                                                button[aria-expanded="false"] > .close{
                                                    display:none;
                                                }
                                                button[aria-expanded="true"] > .open{
                                                    display:none;
                                                }

                                                `
                                            }
                                        </style>
                                        <i className="bi bi-list open fw-bolder text-dark "></i>
                                        <i className="bi bi-x-lg close fw-bolder text-danger "></i>
                                    </button>
                                    <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
                                        <ul className="navbar-nav">
                                            <li className="nav-item me-3">
                                                <NavLink activeclassname='active' className="nav-link fw-semibold textColor texthover" aria-current="page"
                                                    to="/">Home</NavLink>
                                            </li>
                                            <li className="nav-item me-3">
                                                <NavLink className="nav-link fw-semibold textColor texthover"
                                                    to="/project">Projects</NavLink>
                                            </li>
                                            <li className="nav-item">
                                                <NavLink className="nav-link fw-semibold textColor texthover" to="/contact">Contact me</NavLink>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    )
}

export default Navbar
