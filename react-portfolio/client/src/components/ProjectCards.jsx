
// eslint-disable-next-line react/prop-types
const ProjectCards = ({ item }) => {

    return (
        // eslint-disable-next-line react/prop-types
        <div key={item.id} className="card  my-2 mx-2 p-2" style={{ width: "26rem",height:"28rem", background: "#f5f5f9" }}>
            {/* eslint-disable-next-line react/prop-types */}
            <img src={item.projectImg} className="card-img-top" alt="..." />
            <div className="card-body d-flex flex-column  align-items-start justify-content-between">
                {/* eslint-disable-next-line react/prop-types */}
                <span className="card-text p-2"><span style={{ fontWeight: "bold" }}>Project Name:</span>&nbsp;{item.name} </span>
                {/* eslint-disable-next-line react/prop-types */}
                <span className="card-text p-2"><span style={{ fontWeight: "bold" }}>Category:</span>&nbsp;{item.category}</span>
                {/* eslint-disable-next-line react/prop-types */}
                <span className="card-text p-2"><span style={{ fontWeight: "bold" }}>Desc:</span>&nbsp;{item.desc}</span>
                {/* eslint-disable-next-line react/prop-types */}
                <button type="button" className="btns mt-1"><a style={{ textDecoration: "none", color: "white", width: "100%" }} href={item.git}>Source Code</a></button>
            </div>
        </div>
    )
}



export default ProjectCards
