import { Data } from '../constants/data';
import ProjectCards from './ProjectCards'
import { useState } from 'react'


const Projects = () => {

    const buttons = ['HTML-CSS-JS', 'REACTJS', 'MERN']
    const [alldata, setAllData] = useState(Data);
    const [filteredData, setFilteredData] = useState(alldata);

    const filterItem = (category) => {
        const filtered = alldata.filter((item) => item.category === category);
        setFilteredData(filtered);
    }

    return (
        <div id="project" className='p-3 mt-3'>
            <div className="container">
                <div className="row mt-5">
                    <div className="col-lg-12">
                        <h2 className="textColor text-center fw-bolder">PROJECTS</h2>
                    </div>
                </div>
            </div>

            <div className="row justify-content-center mt-3">
                <div className="col-lg-12 justify-content-center d-flex align-items-center flex-wrap p-2">
                    <div className='d-flex align-items-center flex-wrap justify-content-evenly' >
                        <button onClick={() => setFilteredData(alldata)} style={{ margin: "5px", color: "white", fontSize: 12 }} type="button" className="submit btns filterBtn">ALL</button>
                        {
                            buttons.map(btn => (
                                <button key={btn} onClick={() => filterItem(btn)} style={{ margin: "5px", color: "white", fontSize: 12 }} type="button" className="btns submit filterBtn"  >{btn}</button>
                            ))
                        }
                    </div>
                </div>
            </div>
            <div className="container-lg mt-3">
                <div className="row justify-content-center">
                    <div className="col-lg-12 d-flex flex-wrap justify-content-center align-items-center">
                        {
                            filteredData.map(item => (
                                <ProjectCards key={item.id} item={item} />
                            ))
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}


export default Projects
