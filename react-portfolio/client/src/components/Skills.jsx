import bootstrap from '../assets/bootstrap.png';
import css from '../assets/css.png';
import html5 from '../assets/html5.png';
import js from '../assets/js.webp';
import react from '../assets/react.png';
import mongodb from '../assets/mongodb.png';
import node from '../assets/node.png'




const Skills = () => {
    return (
        <section id="skills" className="p-3 mt-3 mb-2" >
            <div className="container">
                <div className="row my-5">
                    <div className="col-lg-12">
                        <h2 className="text-center fw-bolder textColor">SKILLS</h2>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-12">

                        <div id="carouselExampleInterval" className="carousel slide" data-bs-ride="carousel">
                            <div className="carousel-inner">
                                <div className="carousel-item active" data-bs-interval="2000">
                                    <div className="row">
                                        <div className="col-lg-12 d-flex d-flex justify-content-around align-items-center">
                                            <img src={html5} alt="html" className='img-thumbnail p-3 w-25' />
                                            <img src={css} alt="css" className='img-thumbnail p-3 w-25' />
                                            <img src={js} alt="javascript" className='img-thumbnail p-3 w-25' />
                                        </div>
                                    </div>
                                </div>
                                <div className="carousel-item " data-bs-interval="2000">
                                    <div className="row">
                                        <div className="col-lg-12 d-flex d-flex justify-content-around align-items-center">
                                            <img src={bootstrap} alt="bootstrap" className='img-thumbnail p-3 w-25' />
                                            <img src={react} alt="react" className='img-thumbnail p-3 w-25' />
                                            <img src={node} alt="node" className='img-thumbnail p-3 w-25' />
                                        </div>
                                    </div>
                                </div>
                                <div className="carousel-item " data-bs-interval="2000">
                                    <div className="row">
                                        <div className="col-lg-12 d-flex d-flex justify-content-around align-items-center">
                                            <img src={mongodb} alt="mongodb" className='img-thumbnail p-3 w-25' />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )

}

export default Skills
