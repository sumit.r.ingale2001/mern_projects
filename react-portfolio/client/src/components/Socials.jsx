

const Socials = () => {
    return (
        <div className="social-links mt-2">
            <a rel="noreferrer" href="https://www.linkedin.com/in/sumit-ingale-ab3864248/" target="_blank"
                className="fs-3 me-3 text-primary"><i className="bi bi-linkedin"></i></a>
            <a rel="noreferrer" href="https://gitlab.com/sumit.r.ingale2001" target="_blank" className="fs-3 me-3 textColor1"><i
                className="bi bi-github"></i></a>
            <a rel="noreferrer" href="https://www.facebook.com/sumit.ingale.9615" target="_blank" className="fs-3 me-3"><i
                className="bi bi-facebook"></i></a>
            <a rel="noreferrer" href="https://www.instagram.com/_sumit_ingale_02/" target="_blank"
                className="fs-3 text-danger"><i className="bi bi-instagram"></i></a>
        </div>
    )
}

export default Socials
