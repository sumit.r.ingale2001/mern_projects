export const Data = [
    {
        id: 1,
        projectImg: "projectImg/flipkart.png",
        name: "Flipkart Clone",
        category: "MERN",
        desc: "A fullstack flipkart Clone made in mern",
        git: "https://gitlab.com/sumit.r.ingale2001/mern_projects/-/tree/main/flipkart-clone"
    },
    {
        id: 2,
        projectImg: "projectImg/inshortsclone.png",
        name: "Inshorts Clone",
        category: "MERN",
        desc: "Inshorts clone made in mern stack",
        git: "https://gitlab.com/sumit.r.ingale2001/mern_projects/-/tree/main/inshorts-clone"
    },
    {
        id: 3,
        projectImg: "projectImg/crudapp.png",
        name: "Crud application",
        category: "MERN",
        desc: "Crud application made in mern stack",
        git: "https://gitlab.com/sumit.r.ingale2001/mern_projects/-/tree/main/mern-crud-app"
    },
    {
        id: 4,
        projectImg: "projectImg/tinder.png",
        name: "Tinder Clone",
        category: "MERN",
        desc: "A simple tinder clone made in mern stack with swipe functionality",
        git: "https://gitlab.com/sumit.r.ingale2001/mern_projects/-/tree/main/mern-crud-app"
    },
    {
        id: 5,
        projectImg: "projectImg/todolist.png",
        name: "Todo List",
        category: "MERN",
        desc: "Todo list made in mern stack",
        git: "https://gitlab.com/sumit.r.ingale2001/mern_projects/-/tree/main/todo-list"
    },
    {
        id: 28,
        projectImg: "projectImg/googledocs.png",
        name: "Google Docs Clone",
        category: "MERN",
        desc: "Google docs clone made in mern stack",
        git: "https://gitlab.com/sumit.r.ingale2001/mern_projects/-/tree/main/google-docs-clone"
    },
    {
        id: 29,
        projectImg: "projectImg/imagefinder.png",
        name: "Image Finder",
        category: "REACTJS",
        desc: "An image finder application made in react",
        git: "https://gitlab.com/sumit.r.ingale2001/react_projects/-/tree/main/image_finder"
    },
    {
        id: 6,
        projectImg: "projectImg/got.png",
        name: "Game Of Thrones",
        category: "REACTJS",
        desc: "React app that renders all the G.O.T characters",
        git: "https://gitlab.com/sumit.r.ingale2001/react_projects/-/tree/main/game_of_thrones"
    },
    {
        id: 7,
        projectImg: "projectImg/reddit.png",
        name: "Reddit Clone",
        category: "REACTJS",
        desc: "A react js project Reddit Clone",
        git: "https://gitlab.com/sumit.r.ingale2001/react_projects/-/tree/main/reddit"
    },
    {
        id: 8,
        projectImg: "projectImg/imdb.png",
        name: "IMDb Clone",
        category: "REACTJS",
        desc: "A react js project IMDB clone",
        git: "https://gitlab.com/sumit.r.ingale2001/react_projects/-/tree/main/imdb-clone"
    },
    {
        id: 9,
        projectImg: "projectImg/rickmorty.png",
        name: "Rick and Morty App ",
        category: "REACTJS",
        desc: "Rick and morty app which fetches the characters from api",
        git: "https://gitlab.com/sumit.r.ingale2001/react_projects/-/tree/main/rick_morty"
    },
    {
        id: 10,
        projectImg: "projectImg/3d boxes.png",
        name: "3D Boxes",
        category: "HTML-CSS-JS",
        desc: "Html css js based project 3D boxes",
        git: "https://gitlab.com/sumit.r.ingale2001/practice_projects/-/tree/main/3d%20boxes",
    },
    {
        id: 11,
        projectImg: "projectImg/amazon clone.png",
        name: "Amazon Clone",
        category: "HTML-CSS-JS",
        desc: "Html and css responsive project",
        git: "https://gitlab.com/sumit.r.ingale2001/projects/-/tree/main/Amazon_clone",
    },
    {
        id: 12,
        projectImg: "projectImg/blogwebsite.png",
        name: "Blog",
        category: "HTML-CSS-JS",
        desc: "Html and css based responsive project",
        git: "https://gitlab.com/sumit.r.ingale2001/practice_projects/-/tree/main/blog",
    },
    {
        id: 13,
        projectImg: "projectImg/calculator.png",
        name: "Calculator",
        category: "HTML-CSS-JS",
        desc: "Html css js based project calculator",
        git: "https://gitlab.com/sumit.r.ingale2001/practice_projects/-/tree/main/calculator",
    },
    {
        id: 14,
        projectImg: "projectImg/canvas.png",
        name: "Canvas",
        category: "HTML-CSS-JS",
        desc: "html css js based project, on clicking and dragging you can make drawings on the canvas",
        git: "https://gitlab.com/sumit.r.ingale2001/practice_projects/-/tree/main/canvas",
    },
    {
        id: 15,
        projectImg: "projectImg/car-game.png",
        name: "Car Game",
        category: "HTML-CSS-JS",
        desc: "Html css js project retro car game",
        git: "https://gitlab.com/sumit.r.ingale2001/practice_projects/-/tree/main/car%20game",
    },
    {
        id: 16,
        projectImg: "projectImg/drums.png",
        name: "Drums",
        category: "HTML-CSS-JS",
        desc: "Html css js project drums, on clicking on the respective key the audio will play",
        git: "https://gitlab.com/sumit.r.ingale2001/practice_projects/-/tree/main/drum",
    },
    {
        id: 17,
        projectImg: "projectImg/esports.png",
        name: "Esports Website",
        category: "HTML-CSS-JS",
        desc: "Html css bootstrap js based project esports website",
        git: "https://gitlab.com/sumit.r.ingale2001/projects/-/tree/main/Esports",
    },
    {
        id: 18,
        projectImg: "projectImg/flexbox_froggy.png",
        name: "FlexBox Froggy",
        category: "HTML-CSS-JS",
        desc: "Flexbox froggy lvl 1 html css js project",
        git: "https://gitlab.com/sumit.r.ingale2001/practice_projects/-/tree/main/flexbox_froggy",
    },
    {
        id: 19,
        projectImg: "projectImg/grow.png",
        name: "Image Grow",
        category: "HTML-CSS-JS",
        desc: "Html css js project Image grow",
        git: "https://gitlab.com/sumit.r.ingale2001/practice_projects/-/tree/main/img%20grow",
    },
    {
        id: 20,
        projectImg: "projectImg/localstorage.png",
        name: "Local Tapas",
        category: "HTML-CSS-JS",
        desc: "Html css js based project which stores the list into your local storage",
        git: "https://gitlab.com/sumit.r.ingale2001/practice_projects/-/tree/main/local%20storage",
    },
    {
        id: 21,
        projectImg: "projectImg/matchmyfood.png",
        name: "Match my Food game",
        category: "HTML-CSS-JS",
        desc: "Html css js based game match my food",
        git: "https://gitlab.com/sumit.r.ingale2001/projects/-/tree/main/Match%20my%20food%20game",
    },
    {
        id: 22,
        projectImg: "projectImg/myrwa.png",
        name: "MYRWA website",
        category: "HTML-CSS-JS",
        desc: "Html css js bootstrap internshala assesment project",
        git: "https://gitlab.com/sumit.r.ingale2001/projects/-/tree/main/Match%20my%20food%20game",
    },
    {
        id: 23,
        projectImg: "projectImg/photo gallery.png",
        name: "Photo Gallery",
        category: "HTML-CSS-JS",
        desc: "Html css js based project photo gallery which fetches the images",
        git: "https://gitlab.com/sumit.r.ingale2001/practice_projects/-/tree/main/Photo_Gallery",
    },
    {
        id: 24,
        projectImg: "projectImg/remine-india.png",
        name: "Remine India website",
        category: "HTML-CSS-JS",
        desc: "Html css js bootstrap internshala assesment project",
        git: "https://gitlab.com/sumit.r.ingale2001/practice_projects/-/tree/main/Photo_Gallery",
    },
    {
        id: 25,
        projectImg: "projectImg/spotify clone.png",
        name: "Spotify Clone",
        category: "HTML-CSS-JS",
        desc: "Html css js based project Spotify clone",
        git: "https://gitlab.com/sumit.r.ingale2001/projects/-/tree/main/spotify",
    },
    {
        id: 26,
        projectImg: "projectImg/whack_a_mole.png",
        name: "Whack a mole game",
        category: "HTML-CSS-JS",
        desc: "Html css js based project whack a mole",
        git: "https://gitlab.com/sumit.r.ingale2001/practice_projects/-/tree/main/whack%20a%20mole",
    },
    {
        id: 27,
        projectImg: "projectImg/tictactoe.png",
        name: "Tic Tac Toe game",
        category: "HTML-CSS-JS",
        desc: "Html css js based project tic tac toe game",
        git: "https://gitlab.com/sumit.r.ingale2001/practice_projects/-/tree/main/tic-tac-toe",
    },


]

export const lang = [
    {
        id: 1,
        url: "langImg/html5.png"
    },
    {
        id: 2,
        url: "langImg/css.png"
    },
    {
        id: 3,
        url: "langImg/js.webp"
    },
    {
        id: 4,
        url: "langImg/bootstrap.png"
    },
    {
        id: 5,
        url: "langImg/react.png"
    },
    {
        id: 5,
        url: "langImg/mongodb.png"
    },
]