import axios from 'axios';

const URL = "https://portfoliobackend-9lrn.onrender.com";

export const addUser = async (data)=>{
    try {
        return await axios.post(`${URL}/contact`, data)
    } catch (error) {
        console.log(error.message)
    }
}