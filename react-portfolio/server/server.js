import express from 'express';
import Connection from './database/db.js';
import route from './route/route.js';
import cors from 'cors';
import dotenv from 'dotenv'

dotenv.config();

const USERNAME = process.env.DB_USERNAME;
const PASSWORD = process.env.DB_PASSWORD;

const PORT = process.env.PORT || 8000;
const URL = process.env.MONGODB_URI || `mongodb+srv://${USERNAME}:${PASSWORD}@portfolio.mjsbqom.mongodb.net/SUMIT-PORTFOLIO?retryWrites=true&w=majority`;

const app = express();
app.use(express.json());
app.use(cors())
app.use("/", route);
Connection(URL);

app.listen(PORT, () => console.log(`successfully listening to post ${PORT}`))
