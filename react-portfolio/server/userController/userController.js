import User from "../models/userSchema.js";

export const addUser = async (req, res) => {
    try {
        const user = req.body;
        const newUser = new User(user)
        await newUser.save();
        res.status(201).json(newUser);
    } catch (error) {
        res.status(500).json({ message: error.message }, "error while saving data to databse")
    }
}
