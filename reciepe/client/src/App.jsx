
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Navbar from './components/Navbar'
import Home from './components/Home'
import CreateReciepe from './components/CreateReciepe'
import SavedReciepe from './components/SavedReciepe'
import Auth from './components/Auth'
function App() {

  return (
    <>
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route exact path="/add" element={<CreateReciepe />} />
          <Route exact path="/saved" element={<SavedReciepe />} />
          <Route exact path="/auth" element={<Auth />} />
        </Routes>
      </BrowserRouter>
    </>
  )
}

export default App
