/* eslint-disable no-unused-vars */
import { useState } from "react"
import { loginUser, registerUser } from "../services/api";
import { useNavigate } from "react-router-dom";
import { useCookies } from 'react-cookie'

const Auth = () => {
    const navigate = useNavigate()
    const [toggleAccount, setToggleAccount] = useState(false);

    const [cookies, setCookies] = useCookies(["access_token"])

    const [formValues, setFormValues] = useState({
        username: "",
        password: ""
    });

    const handleChange = (e) => {
        setFormValues({ ...formValues, [e.target.name]: e.target.value })
    }

    const handleRegister = async (e) => {
        e.preventDefault();
        const response = await registerUser(formValues)
        if (response) {
            if (response.data.status === "ok") {
                alert(response.data.message)
                navigate("/")
            } else {
                alert(response.data.message)
            }
        } else {
            alert("Something went wrong")
        }
        console.log(response.data)
    }

    const handleLogin = async (e) => {
        e.preventDefault();
        const response = await loginUser(formValues);
        console.log(response)
        if (response) {
            if (response.data.status === "ok") {
                setCookies("access_token", response.data.token)
                window.localStorage.setItem("userID", response.data.userID)
                alert(response.data.message);
                navigate("/")
            } else {
                alert(response.data.message);
            }
        } else {
            alert("Enter correct username or password");
        }
    }


    return (
        <div id="auth" className="d-flex justify-content-center align-items-center" style={{ width: "100vw", height: "100vh" }} >
            {
                toggleAccount ? (
                    <div className="card px-2 py-3" style={{ maxWidth: "600px", width: "90%" }} >
                        <h5 className="text-center" >Register</h5>
                        <div className="card-body mt-3 d-flex flex-column">
                            <div className="input-group mb-3">
                                <input type="text"
                                    name="username"
                                    onChange={handleChange}
                                    className="form-control"
                                    placeholder="Username"
                                    aria-label="Username"
                                    aria-describedby="basic-addon1" />
                            </div>
                            <div className="input-group mb-3">
                                <input type="password"
                                    onChange={handleChange}
                                    name="password"
                                    className="form-control"
                                    placeholder="Password"
                                    aria-label="password"
                                    aria-describedby="basic-addon1" />
                            </div>
                            <button className="btn btn-success mb-3" onClick={handleRegister} >Register</button>
                            <p>Already a member? <span style={{ cursor: "pointer" }} onClick={() => setToggleAccount(false)} className="text-success">Login</span></p>
                        </div>
                    </div>
                ) : (
                    <div className="card px-2 py-3" style={{ maxWidth: "600px", width: "90%" }} >
                        <h5 className="text-center" >Login</h5>
                        <div className="card-body mt-3 d-flex flex-column">
                            <div className="input-group mb-3">
                                <input type="text"
                                    name="username"
                                    onChange={handleChange}
                                    className="form-control"
                                    placeholder="Username"
                                    aria-label="Username"
                                    aria-describedby="basic-addon1" />
                            </div>
                            <div className="input-group mb-3">
                                <input type="password"
                                    onChange={handleChange}
                                    name="password"
                                    className="form-control"
                                    placeholder="Password"
                                    aria-label="password"
                                    aria-describedby="basic-addon1" />
                            </div>
                            <button className="btn mb-3 btn-success" onClick={handleLogin} >Login</button>
                            <p>Not a member? <span style={{ cursor: "pointer" }} onClick={() => setToggleAccount(true)} className="text-success">Signup</span></p>
                        </div>
                    </div>
                )
            }
        </div>
    )
}

export default Auth
