/* eslint-disable no-unused-vars */
import { useState } from "react"
import { createReciepe } from "../services/api"
import { useGetUserID } from '../hooks/useGetUserId'
import { useNavigate } from 'react-router-dom'
import { useCookies } from "react-cookie"


const CreateReciepe = () => {
  const userID = useGetUserID();

  const [cookies, setCookies] = useCookies(["access_token"])
  const navigate = useNavigate()

  const [reciepe, setreciepe] = useState({
    name: "",
    ingredients: [],
    instructions: "",
    cookingTime: 0,
    imgUrl: "",
    userOwner: userID
  })


  const handleChange = (e) => {
    setreciepe({ ...reciepe, [e.target.name]: e.target.value })
  }

  const addIngredient = () => {
    setreciepe({ ...reciepe, ingredients: [...reciepe.ingredients, ""] });
  }

  const handleIngredientChange = (e, idx) => {
    const { value } = e.target;
    const ingredients = [...reciepe.ingredients];
    ingredients[idx] = value;
    setreciepe({ ...reciepe, ingredients });
  }


  const submitReciepe = async (e) => {
    e.preventDefault();
    if (!cookies.access_token) {
      return alert("Login to create reciepe")
    }
    if (reciepe.cookingTime && reciepe.imgUrl, reciepe.ingredients && reciepe.name && reciepe.instructions !== "") {
      const response = await createReciepe(reciepe);
      if (response) {
        alert("reciepe created");
        navigate("/")
      }
    } else {
      alert("fields cannot be empty")
    }
  }

  return (
    <div id="create-reciepe" className="d-flex justify-content-center align-items-center" style={{ minHeight: "100vh", width: "100vw" }}>

      <div className="card p-3" style={{ maxWidth: "600px", width: "80%" }} >
        <h3 className="text-center mb-1" >Create reciepe</h3>

        <div className="card-body d-flex flex-column">

          <div className="input-group mb-3">
            <input
              required
              type="text"
              name="name"
              onChange={handleChange}
              className="form-control"
              placeholder="Name"
              aria-label="Name"
              aria-describedby="basic-addon1" />
          </div>

          <div className="input-group mb-3 flex-column d-flex justify-content-center align-items-center">
            {reciepe.ingredients.map((ingredient, idx) => (
              <input
                required
                type="text"
                style={{ width: "100%" }}
                className="form-control mb-3"
                key={idx}
                aria-describedby="basic-addon1"
                placeholder="Ingredients"
                aria-label="Ingredients"
                name="ingredients"
                value={ingredient}
                onChange={(e) => handleIngredientChange(e, idx)}
              />
            ))}
          </div>

          <button className="btn btn-success mb-3" onClick={addIngredient} >Add ingredients</button>
          <div className="input-group mb-3">
            <input
              required
              type="text"
              name="instructions"
              onChange={handleChange}

              className="form-control"
              placeholder="Instructions"
              aria-label="Instructions"
              aria-describedby="basic-addon1" />
          </div>

          <div className="input-group mb-3">
            <input
              required
              type="text"
              name="imgUrl"
              onChange={handleChange}
              className="form-control"
              placeholder="Image url"
              aria-label="ImageUrl"
              aria-describedby="basic-addon1" />
          </div>

          <div className="input-group mb-3">
            <input
              required
              type="number"
              name="cookingTime"
              onChange={handleChange}
              className="form-control"
              placeholder="Cooking time (minutes)"
              aria-label="ImageUrl"
              aria-describedby="basic-addon1" />
          </div>
          <button className="btn btn-success" onClick={submitReciepe} >Submit</button>
        </div>
      </div>
    </div>
  )
}

export default CreateReciepe
