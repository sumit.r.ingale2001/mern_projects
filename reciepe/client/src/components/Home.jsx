/* eslint-disable no-unused-vars */
import { useEffect, useState } from "react";
import { getReciepes, savedReciepes } from "../services/api";
import { useGetUserID } from '../hooks/useGetUserId'
import axios from "axios";

const Home = () => {

    const userID = useGetUserID()

    const [reciepes, setReciepes] = useState([])
    const [savedReciepe, setSavedReciepes] = useState([])

    useEffect(() => {
        const fetchReciepes = async () => {
            const response = await getReciepes();
            setReciepes(response.data.reciepes);
        }

        const fetchSavedreciepe = async () => {
            const response = await savedReciepes(userID)
            console.log(response)
            setSavedReciepes(response.data.savedReciepes);
        }

        fetchReciepes()
        fetchSavedreciepe()
    }, []);


    const save = async (reciepeID) => {
        try {
            const response = await axios.put('http://localhost:8000/recipes', { reciepeID, userID })
            if (response) {
                if (response.data.status === "ok") {
                    setSavedReciepes(response.data.savedReciepes);
                    alert(response.data.message)
                } else {
                    alert(response.data.message)
                }
            }
        } catch (error) {
            console.log(error)
        }
    }

    const isreciepeSaved = (id) => savedReciepe.includes(id)
    return (
        <>
            <div className="container mt-5 pt-5">
                <div className="row justify-content-center">
                    <div className="col-lg-12 d-flex gap-3 flex-wrap">
                        {
                            reciepes.map((reciepe) => (
                                <div className="card py-3 px-2" key={reciepe._id} style={{ maxWidth: "20rem", width: "80%", }}>
                                    <h5 className="text-center" >{reciepe.name}</h5>
                                    <img className="card-img-top" style={{ objectFit: "cover", height: "200px" }} src={reciepe.imgUrl} alt={reciepe.name} />
                                    <div className="card-body d-flex flex-column justify-content-between">
                                        <p className="card-text">{reciepe.instructions}</p>
                                        <p className="card-text">Cooking time: {reciepe.cookingTime} minutes</p>
                                    </div>
                                    <button
                                        disabled={isreciepeSaved(reciepe._id)}
                                        onClick={() => save(reciepe._id)} className="btn btn-success">{isreciepeSaved(reciepe._id) ? "Saved":"Save"}</button>
                                </div>
                            ))
                        }
                    </div>
                </div>
            </div>
        </>
    )
}

export default Home
