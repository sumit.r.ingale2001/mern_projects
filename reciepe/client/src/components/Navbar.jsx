import { Link, useNavigate } from "react-router-dom"
import { useCookies } from "react-cookie"


const Navbar = () => {

    const navigate = useNavigate()

    const [cookies, setCookies] = useCookies(["access_token"])

    const logOutUser = (e) => {
        e.preventDefault();
        alert("Logged out successfully")
        setCookies("access_token", "");
        window.localStorage.removeItem("userID")
        navigate("/auth")
    }

    return (
        <>
            <div className="container-lg fixed-top">
                <div className="row">
                    <div className="col-lg-12">
                        <nav className="navbar navbar-expand-lg bg-body-tertiary">
                            <div className="container-fluid">
                                <Link className="navbar-brand" to="/">ReciepeApp</Link>
                                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <span className="navbar-toggler-icon"></span>
                                </button>
                                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                                        <li className="nav-item">
                                            <Link className="nav-link active" aria-current="page" to="/">Home</Link>
                                        </li>
                                        <li className="nav-item">
                                            <Link className="nav-link" to="/saved">Saved Reciepe</Link>
                                        </li>
                                        <li className="nav-item">
                                            <Link className="nav-link" to="/add">Create Reciepe</Link>
                                        </li>
                                    </ul>
                                    <form className="d-flex" role="search">
                                        <button className="btn btn-success" type="submit">
                                            {
                                                cookies.access_token ? (
                                                    <span onClick={logOutUser}  >Logout</span>
                                                ) : (
                                                    <Link to="/auth" className="text-light" >Login/register</Link>
                                                )
                                            }
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Navbar
