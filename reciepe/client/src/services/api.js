import axios from 'axios'

const url = "http://localhost:8000";

export const registerUser = async (data) => {
    try {
        return await axios.post(`${url}/auth/register`, data)
    } catch (error) {
        console.log(error, 'error while calling registerUser api');
    }
}


export const loginUser = async (data) => {
    try {
        return await axios.post(`${url}/auth/login`, data)
    } catch (error) {
        console.log("error while calling login user api", error);
    }
}

export const createReciepe = async (data) => {
    try {
        return await axios.post(`${url}/recipes/create`, data)
    } catch (error) {
        console.log(error, "error while calling create reciepe api")
    }
}

export const getReciepes = async () => {
    try {
        return await axios.get(`${url}/recipes`);
    } catch (error) {
        console.log(error, 'error while calling getReciepes api')
    }
}

export const saveReciepe = async (id, userID) => {
    try {
        const response = await axios.put(`${url}/recipes`, { id, userID })
        console.log(response, "saveReciepe api function")
    } catch (error) {
        console.log(error, 'error while saving the reciepe')
    }
}

export const savedReciepes = async (userID) => {
    try {
        return await axios.get(`${url}/ids/${userID}`)
    } catch (error) {
        console.log(error, 'error while calling savedReciepe api');
    }
}