import Reciepe from '../models/reciepes.js';
import User from '../models/user.js';


export const getReciepes = async (req, res) => {
    try {
        const data = await Reciepe.find();
        if (data) {
            return res.status(200).json({ message: "Reciepes found", status: "ok", reciepes: data });
        } else {
            return res.json({ message: "Reciepes not found", status: "not ok" })
        }
    } catch (error) {
        res.status(500).json(error, "error while getting the reciepes");
    }
}


export const createReciepe = async (req, res) => {
    try {
        const data = req.body;
        const newReciepe = new Reciepe(data);
        await newReciepe.save();
        res.status(200).json(newReciepe)
    } catch (error) {
        res.status(500).json(error, 'error while creating a new reciepe')
    }
}

export const saveReciepe = async (req, res) => {
    try {
        const reciepe = await Reciepe.findById(req.body.reciepeID);
        const user = await User.findById(req.body.userID);

        user.savedReciepes.push(reciepe);
        await user.save();

        res.json({ message: "Reciepe saved successfully", status: "ok", savedReciepes: user.savedReciepes })
    } catch (error) {
        res.status(500).json(error, "error while updating the reciepe")
    }
}


export const savedReciepes = async (req, res) => {
    try {
        const user = await User.findById(req.params.userID);
        if (user) {
            return res.json({ savedReciepes: user?.savedReciepes, status: "ok" });
        } else {
            return res.json({ message: "No saved reciepes found", status: "not ok" })
        }
    } catch (error) {
        res.status(500).json(error, "error while getting the saved reciepes")
    }
}

export const getAllSavedReciepes = async (req, res) => {
    try {
        const user = await User.findById(req.body.userID);
        const savedReciepes = await Reciepe.find({
            _id: { $in: user.savedReciepes }
        })
        res.json(savedReciepes)
    } catch (error) {
        res.json(error, "error while getiing all saved reciepes");
    }
}