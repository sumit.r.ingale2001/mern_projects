import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import User from '../models/user.js';


const hashedPass = async (pass) => {
    return await bcrypt.hash(pass, 10);
}

const comparePass = async (pass, databsePass) => {
    return await bcrypt.compare(pass, databsePass);
}


export const registerUser = async (req, res) => {
    try {
        const { username, password } = req.body;
        const exist = await User.findOne({ username });

        if (exist) {
            return res.json({ message: "User already exist", status: "not ok" });
        } else {
            const hashedPassword = await hashedPass(password);
            const newUser = new User({
                username,
                password: hashedPassword
            })
            await newUser.save();
            return res.json({ message: "Account created successfully", status: "ok", data: newUser })
        }

    } catch (error) {
        res.status(500).json(error, "error while registering user")
    }
}

export const loginUser = async (req, res) => {
    try {
        const { username, password } = req.body;
        const user = await User.findOne({ username });
        if (!user) {
            return res.json({ message: "User doesn't exist", status: "not ok" });
        } else {
            const isValid = await comparePass(password, user.password);
            if (isValid) {
                const token = jwt.sign({ id: user._id }, 'secret-key', { expiresIn: "5d" })
                return res.json({ token, userID: user._id, status: "ok", message: "Logged in successfully" })

            } else {
                return res.json({ message: "Enter correct username and password", status: "not ok" })
            }
        }
    } catch (error) {
        res.status(500).json(error, "error while logging in the user")
    }
}