import mongoose from 'mongoose';


const url = "mongodb://127.0.0.1:27017/reciepeApp";

const Connection = async () => {
    try {
        await mongoose.connect(url, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
        console.log("Database connected successfully")
    } catch (error) {
        console.log(error, 'error while connecting to database');
    }
}

export default Connection;