import express from 'express';
import cors from 'cors'
import Connection from './database/db.js';
import userRoutes from './routes/user.js'
import reciepeRoutes from './routes/reciepe.js' 
const PORT = 8000;
const app = express();

app.use(express.json());
app.use(cors());



app.use("/auth", userRoutes)
app.use("/recipes", reciepeRoutes)

Connection()
app.listen(PORT, () => console.log(`server running on port ${PORT}`))
