import mongoose from 'mongoose';

const reciepeSchema = new mongoose.Schema({
    name: { type: String, required: true },
    ingredients: [{ type: String, required: true }],
    instructions: { type: String, required: true },
    imgUrl: { type: String, required: true },
    cookingTime: { type: Number, required: true },
    userOwner: { type: mongoose.Schema.Types.ObjectId, required: true, ref: "user" },
})

const Reciepe = mongoose.model("reciepe", reciepeSchema);
export default Reciepe;