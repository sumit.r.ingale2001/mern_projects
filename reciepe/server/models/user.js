import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    savedReciepes:[{type:mongoose.Schema.Types.ObjectId, ref:"reciepe"}]
})

const User = mongoose.model("user", userSchema);
export default User;