import { Router } from "express";
import { createReciepe, getReciepes, saveReciepe, savedReciepes } from "../controllers/reciepe.js";

const router = Router();


router.get("/", getReciepes);
router.post("/create", createReciepe);
router.put("/", saveReciepe);
router.get("/ids/:userID", savedReciepes)


export default router;