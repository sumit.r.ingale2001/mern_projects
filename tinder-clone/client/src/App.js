import './App.css';
import Header from './components/Header';
import SwipeButtons from './components/SwipeButtons';
import TinderCards from './components/TinderCards';

function App() {
  return (
    <div className="App">
      {/* header start */}
      <Header/>
      {/* header end */}

      {/* tinder cards start  */}
      <TinderCards/>
      {/* tinder cards end */}

      {/* swipe buttons start */}
      <SwipeButtons/>
      {/* swipe buttons end */}
    </div>
  );
}

export default App;
