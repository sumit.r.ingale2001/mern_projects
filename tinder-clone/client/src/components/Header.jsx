import React from 'react'
import PersonIcon from '@mui/icons-material/Person';
import IconButton from "@mui/material/IconButton";
import ForumIcon from "@mui/icons-material/Forum"
const Header = () => {
    return (
        <div className='header'>
            <IconButton>
                <PersonIcon />
            </IconButton>

            <img className='header__logo' src="https://www.tinderpressroom.com/image/flame-gradient-RGB_tn1100-category.png" alt="" />

            <IconButton>
                <ForumIcon/>
            </IconButton>
        </div>
    )
}

export default Header
