import React, { useEffect, useState } from 'react';
import TinderCard from "react-tinder-card";
import axios from '../axios'


const TinderCards = () => {
    const [people, setPeople] = useState([]);

    useEffect(  ()=>{
        async function fetchData(){
            const req = await axios.get("/cards")
            setPeople(req.data);
        }
        fetchData();
    },[]);

    const swiped = (direction, nameToDelete) => {
        console.log("removing:" +  nameToDelete )

    }
    const outOfScreen = (name) => {
        console.log(name + 'left the screen')
    }
    return (
        <div className='tinderCards'>

            <div className='tinderCards__cardContainer'>
                {people.map((person) => (
                    <TinderCard
                        className='swipe'
                        key={person.imgUrl}
                        preventSwipe={['up', 'down']}
                        onSwipe={(dir) => swiped(dir, person.name)}
                        onCardLeftScreen={() => outOfScreen(person.name)}
                    >
                        <div className="card" style={{backgroundImage:`url(${person.imgUrl})`}}>
                        <h3>{person.name}</h3>
                        </div>
                    </TinderCard>
                ))}
            </div>
        </div>
    )
}

export default TinderCards
