import express from 'express';
import cors from 'cors';
import mongoose from 'mongoose';
import Cards from "./dbCards.js"


// App config 
const app = express();
const port = process.env.PORT || 8000;
app.use(express.json());
app.use(cors());
const url = "mongodb+srv://sumitringale2001:tinderclone@tinder-clone.ysw7khq.mongodb.net/TINDER-CLONE?retryWrites=true&w=majority";
// Middlewares 



// db config 
mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log("DB connected")).catch((err) => console.log("DB not connected", err))


// api endpoints 
app.get("/", async (req, res) => {
    try {
        res.status(200).send("hello")
    } catch (error) {
        res.status(500).send(error)
    }
})

app.post("/cards", async (req, res) => {

        const dbCard = req.body
        try {
            const response = await Cards.create(dbCard);
            res.status(201).send(response)
        } catch (error) {
            res.status(500).send(err)
        }
})


app.get("/cards", async  (req, res) => {
    try {
        const response = await Cards.find({});
        res.status(200).send(response)
    } catch (error) {
        res.status(500).send(error)
    }
})

// listener 
app.listen(port, () => console.log(`Successfully listening to port ${port}`));